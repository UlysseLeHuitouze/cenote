#include <string.h>
#include "cenote.h"



static VkInstance instance;
static VkDebugUtilsMessengerEXT debugMessenger;
static VkPhysicalDevice physicalDevice;
static VkDevice device;
static VkSwapchainCreateInfoKHR swapchainCreateInfoKhr = {
        .sTypeDefault(VkSwapchainCreateInfoKHR),
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .minImageCount = CE_FRAME_COUNT,
        .imageArrayLayers = 1,
        .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .clipped = VK_TRUE
};
static VkImage swapchainImages[CE_FRAME_COUNT];
static VkImageView swapchainImageViews[CE_FRAME_COUNT];
static VkFramebuffer framebuffers[CE_FRAME_COUNT];
static uint32_t framebuffer_attachment_count;
static VkImageView *framebuffer_attachments;
static VkQueue mainQueue;
static VkQueue secondaryQueue;
static uint32_t queueFamilyIndex;
static VkRenderPass renderPass;
static VmaAllocator allocator;

#define INLINE inline __attribute((always_inline))

#ifdef CENOTE_INLINE
#   define MAYBE_INLINE INLINE
#else
#   define MAYBE_INLINE
#endif //CENOTE_INLINE



// getters

extern INLINE VkInstance cenote_get_instance()
{
    return instance;
}

extern INLINE VkDebugUtilsMessengerEXT cenote_get_debug_messenger()
{
    return debugMessenger;
}

extern INLINE VkPhysicalDevice cenote_get_physical_device()
{
    return physicalDevice;
}

extern INLINE VkDevice cenote_get_device()
{
    return device;
}

extern INLINE VkSwapchainCreateInfoKHR* cenote_get_swapchain_info()
{
    return &swapchainCreateInfoKhr;
}

extern INLINE VkSwapchainKHR cenote_get_swapchain()
{
    return swapchainCreateInfoKhr.oldSwapchain;
}

extern INLINE VkImage cenote_get_swapchain_image(uint32_t index)
{
    return swapchainImages[index];
}

extern INLINE VkImageView cenote_get_swapchain_image_view(uint32_t index)
{
    return swapchainImageViews[index];
}

extern INLINE VkFramebuffer cenote_get_framebuffer(uint32_t index)
{
    return framebuffers[index];
}

extern INLINE VkRenderPass cenote_get_render_pass()
{
    return renderPass;
}

extern INLINE uint32_t cenote_get_swapchain_width()
{
    return swapchainCreateInfoKhr.imageExtent.width;
}

extern INLINE uint32_t cenote_get_swapchain_height()
{
    return swapchainCreateInfoKhr.imageExtent.height;
}

extern INLINE VmaAllocator cenote_get_allocator()
{
    return allocator;
}


extern INLINE VkSurfaceKHR cenote_get_surface()
{
    return swapchainCreateInfoKhr.surface;
}


extern INLINE VkQueue cenote_get_main_queue()
{
    return mainQueue;
}


extern INLINE VkQueue cenote_get_secondary_queue()
{
    return secondaryQueue;
}


extern INLINE uint32_t cenote_get_queue_family_index()
{
    return queueFamilyIndex;
}


extern INLINE void cenote_set_render_pass(VkRenderPass vkRenderPass)
{
    renderPass = vkRenderPass;
}

extern INLINE void cenote_set_framebuffers_attachment_count(uint32_t attachmentCount)
{
    free(framebuffer_attachments);
    // attachments contain 1 more slot for swapchain image views
    framebuffer_attachment_count = attachmentCount + 1;
    // calloc to initialize attachments to VK_NULL_HANDLE, for cenote_set_framebuffers_attachments not to segfault.
    framebuffer_attachments = ncalloc(framebuffer_attachment_count, sizeof(VkImageView));
}

extern INLINE void cenote_set_framebuffers_attachments(const VkImageView *pAttachments)
{
    // attachments' last slot is reserved for swapchain image views
    for (uint32_t i = 0; i < framebuffer_attachment_count - 1; i++)
        ceDestroyImageView(framebuffer_attachments[i]);
    memcpy(framebuffer_attachments, pAttachments, sizeof(VkImageView) * (framebuffer_attachment_count - 1));
}

/**
 * The graphic and presentation queue should be the same for optimal performance.
 * @param props VkQueueFamilyProperties
 * @param featuresSurface whether the queue features surface-related operations
 * @return whether the queue family is deemed suited for both graphics and presentation
 */
static bool isGraphicPresentQueue(VkQueueFamilyProperties props, VkBool32 featuresSurface)
{
    return (props.queueFlags & VK_QUEUE_GRAPHICS_BIT) && featuresSurface;
}





extern MAYBE_INLINE void cenote_use(CenoteCreateInfo *createInfo, QueueFeatures *outputQueues) {

    const bool useCenoteQueues = outputQueues == NULL;
    QueueFamilySelectInfo *queueFamilySelectInfo = createInfo->requestedQueuesInfo;

    QueueFeatures cenoteFeatures = {
            .queues = (VkQueue[2]) {},
            .queueFamilyIndices = (uint32_t[2]) {}
    };
    QueueFamilySelectInfo cenoteFamilies = {
            .queueFamiliesSelectPredicate = (bool (*[2])(VkQueueFamilyProperties, VkBool32)){
                    isGraphicPresentQueue, isGraphicPresentQueue
            },
            .queueFamiliesFallback = ca_array_of(uint32_t, -1, 0)
    };

    if (useCenoteQueues) {
        if (queueFamilySelectInfo != NULL)
            CA_WARN("cenote_use's createInfo->requestedQueuesInfo must be NULL when cenote_use's outputQueues is");

        outputQueues = &cenoteFeatures;
        queueFamilySelectInfo = &cenoteFamilies;
    }



    swapchainCreateInfoKhr.imageExtent.width = createInfo->windowWidth;
    swapchainCreateInfoKhr.imageExtent.height = createInfo->windowHeight;

    VulkanSetupInput input = {
            .appInfo = &(VkApplicationInfo) {
                .sTypeDefault(VkApplicationInfo),
                .pApplicationName = createInfo->appName,
                .applicationVersion = createInfo->appVersion,
                .pEngineName = "Cenote engine",
                .engineVersion = VK_MAKE_VERSION(1, 0, 0),
                .apiVersion = createInfo->apiVersion
            },
            .requestedLayers = createInfo->requestedLayers,
            .requestedInstanceExtensions = createInfo->requestedInstanceExtensions,
            .requestedDeviceExtensions = createInfo->requestedDeviceExtensions,
            .pNextDeviceCreateInfo = createInfo->pNextDeviceCreateInfo,
            .GPUName = createInfo->GPUName,
            .getInstanceExtensions_ptr = createInfo->getInstanceExtensions_ptr,
            .createVkSurfaceKHR_ptr = createInfo->createVkSurfaceKHR_ptr,
            .requestedDeviceFeatures = createInfo->requestedDeviceFeatures,
            .requestedQueuesInfo = queueFamilySelectInfo,
            .requestedSwapchainInfo = createInfo->requestedSwapchainInfo,
            .requestedAllocatorInfo = createInfo->requestedAllocatorInfo == NULL ?
                    &(VmaAllocatorCreateInfo){} : createInfo->requestedAllocatorInfo
    };
    input.requestedSwapchainInfo->imageCount = CE_FRAME_COUNT;

    VulkanSetupOutput output = {
            .instance = &instance,
            .debugMessenger = &debugMessenger,
            .physicalDevice = &physicalDevice,
            .device = &device,
            .queueFeatures = outputQueues,
            .swapchainInfo = &swapchainCreateInfoKhr,
            .allocator = &allocator
    };

    ceinitvk(&input, &output);

    if (useCenoteQueues) {

        BEXIT(outputQueues->capacity != 2 /*length of queueFamiliesFallback array*/,
              "Internal error, incoherent number of queues: Expected 2, got %d", outputQueues->capacity);

        BEXIT(
                outputQueues->queueFamilyIndices[0] != outputQueues->queueFamilyIndices[1],
                "Illegal state: GPU failed to provide same queue family for the 2 cenote queues. That shouldn't happen"
        );

        BEXIT(
                !outputQueues->queues[0] || !outputQueues->queues[1],
                "GPU does not support 2 different queues for queue family #%d",
                outputQueues->queueFamilyIndices[0]
        );

        uint32_t propCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &propCount, VK_NULL_HANDLE);
        VkQueueFamilyProperties *props = smalloc(sizeof *props * propCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &propCount, props);

        VkQueueFamilyProperties prop = props[outputQueues->queueFamilyIndices[0]];

        VkBool32 surfaceSupported = VK_FALSE;
        vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice,
                                             outputQueues->queueFamilyIndices[0],
                                             cenote_get_surface(), &surfaceSupported);

        BEXIT(!(prop.queueFlags & VK_QUEUE_GRAPHICS_BIT && surfaceSupported),
              "GPU does not offer a queue family capable of both graphics and presentation.");

        free(props);

        mainQueue = outputQueues->queues[0];
        secondaryQueue = outputQueues->queues[1];
        queueFamilyIndex = outputQueues->queueFamilyIndices[0];
    }

}


void cenote_on_resize(uint32_t width, uint32_t height)
{
    swapchainCreateInfoKhr.imageExtent.width = width;
    swapchainCreateInfoKhr.imageExtent.height = height;


    cemakeswapchain(physicalDevice, device, &swapchainCreateInfoKhr, NULL, swapchainImages);

    VkImageViewCreateInfo imageViewCreateInfo = {
            .sTypeDefault(VkImageViewCreateInfo),
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = swapchainCreateInfoKhr.imageFormat,
            .subresourceRange = {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    .levelCount = 1,
                    .layerCount = 1
            }
    };
    VkFramebufferCreateInfo framebufferCreateInfo = {
            .sTypeDefault(VkFramebufferCreateInfo),
            .renderPass = renderPass,
            .width = width,
            .height = height,
            .layers = 1,
            .attachmentCount = framebuffer_attachment_count,
    };

    for (uint32_t i = 0; i < CE_FRAME_COUNT; i++)
    {
        ceDestroyImageView(swapchainImageViews[i]);
        imageViewCreateInfo.image = swapchainImages[i];
        ceCreateImageView(&imageViewCreateInfo, swapchainImageViews + i);

        ceDestroyFramebuffer(framebuffers[i]);
        framebuffer_attachments[framebuffer_attachment_count - 1] = swapchainImageViews[i];
        framebufferCreateInfo.pAttachments = framebuffer_attachments;
        ceCreateFramebuffer(&framebufferCreateInfo, framebuffers + i);
    }
}


void cenote_close()
{
    for (uint32_t i = 0; i < framebuffer_attachment_count - 1; i++)
    {
        ceDestroyImageView(framebuffer_attachments[i]); // last slot is swapchain image view, will be destroyed later
    }
    free(framebuffer_attachments);

    for (uint32_t i = 0; i < CE_FRAME_COUNT; i++)
    {
        ceDestroyFramebuffer(framebuffers[i]);
        ceDestroyImageView(swapchainImageViews[i]);
    }
    ceDestroySwapchainKHR(cenote_get_swapchain());

    ceDestroyRenderPass(renderPass);

    vmaDestroyAllocator(allocator);
    cedestroyvk(instance, NULL, debugMessenger, swapchainCreateInfoKhr.surface, device);
}
