#include "cemaths.h"
#include <math.h>
#include <stdio.h>

#define SSE 1
#define SSE2 2
#define SSE3 4
#define SSSE3 8
#define SSE41 16
#define SSE42 32

#define AVX 64
#define AVX2 128

#define FMA 256

#if defined(__FMA__)
#define CONF FMA
#elif defined(__AVX2__)
#define CONF AVX2
#elif defined(__AVX__)
#define CONF AVX
#elif defined(__SSE42__)
#define CONF SSE42
#elif defined(__SSE41__)
#define CONF SSE41
#elif defined(__SSSE3__)
#define CONF SSSE3
#elif defined(__SSE3__)
#define CONF SSE3
#elif defined(__SSE2__)
#define CONF SSE2
#elif defined(__SSE__)
#define CONF SSE
#else
#define CONF 0
#endif

#if CONF >= SSE
    #include "immintrin.h"
#endif

#define SHUFFLE2(u, v, mask) __builtin_shuffle((u), (v), (mask))
#define SHUFFLE(u, mask) __builtin_shuffle((u), (mask))

#define v2_mask(a, b) (ivec2(a, b).gen)
#define v3_mask(a, b, c) (ivec3(a, b, c).gen)
#define v4_mask(a, b, c, d) (ivec4(a, b, c, d).gen)
#define v3C(u) (const vec3_t)(u)
#define v4C(u) (const vec4_t)(u)

/**
 * Unsafe version of normalize for vec3_t.
 * @param u expected to be exactly vec4_t_g
 * @return a const vec3_t
 */
#define VEC3_NORMALIZE(u) v3C(normalize(v4C(u)).gen)

/**
 * Unsafe version of dot for vec4_t_g.
 * @param u expected to be exactly vec4_t_g
 * @param v expected to be exactly vec4_t_g
 * @return the dot product as a float
 */
#define VEC3_DOT(u, v) dot(v4C(u), v4C(v))




extern inline __attribute((always_inline)) vec3_t cross(vec3_t u, vec3_t v)
{
    const ivec4_t_g mask1 = v4_mask(1, 2, 0, -1);
    const ivec4_t_g mask2 = v4_mask(2, 0, 1, -1);

    const vec4_t_g r1 = SHUFFLE(u.gen, mask1);
    const vec4_t_g r2 =  SHUFFLE(v.gen, mask2);

    const vec4_t_g l1 = SHUFFLE(v.gen, mask1);
    const vec4_t_g l2 = SHUFFLE(u.gen, mask2);

    return v3C(r1 * r2 - l1 * l2);
}


extern inline __attribute((always_inline)) float dot(vec4_t u, vec4_t v)
{
#if CONF >= SSE41
    return _mm_cvtss_f32(_mm_dp_ps((__m128)u.gen, (__m128)v.gen, 0xff));
#else
    const vec4_t_g m = u.gen * v.gen;
    const vec4_t_g p = SHUFFLE(m, v4_mask(0, 2, -1, -1)) + SHUFFLE(m, v4_mask(1, 3, -1, -1));
    return p[0] + p[1];
#endif
}


extern inline __attribute((always_inline)) vec4_t normalize(vec4_t u)
{
#if CONF >= SSE42
    const __m128 d = _mm_dp_ps((__m128)u, (__m128)u, 0xff);
    const __m128 r = _mm_rsqrt_ps(d);
    return u * (vec4_t)r;
#elif CONF >= SSE
    float d = dot(u, u);
    const __m128 r = _mm_rsqrt_ps((__m128){d, d, d, d});
    return v4C(_mm_mul_ps(u.gen, r));
#else
    float d = dot(u, u);
    return v4C(u.gen / (float)sqrt((double) d));
#endif
}

extern inline __attribute((always_inline)) void perspective_lh(mat4_t m, float fov, uint32_t width, uint32_t height,
                                                            float z_near, float z_far)
{
    float r_fov = (float)(1.0 / tan(0.5 * radians(fov)));
    float r_z_delta = (float)((double) z_far / (double)(z_far - z_near));

    m[0].x = r_fov * ((float) height / (float) width);
    m[0].y = m[0].z = m[0].w = 0;

    m[1].y = r_fov;
    m[1].x = m[1].z = m[1].w = 0;

    m[2].z = r_z_delta;
    m[2].w = 1;
    m[2].x = m[2].y = 0;

    m[3].z = - r_z_delta * z_near;
    m[3].x = m[3].y = m[3].w = 0;
}

extern inline __attribute((always_inline)) void nperspective(mat4_t m, float reverse_tan_half_fov, float reverse_aspect_ratio)
{
    m[0].x = reverse_tan_half_fov * reverse_aspect_ratio;
    m[1].y = reverse_tan_half_fov;
}

extern inline __attribute((always_inline)) void perspective_rh(mat4_t m, float fov, uint32_t width, uint32_t height,
                                                               float z_near, float z_far)
{
    float r_fov = (float)(1.0 / tan(0.5 * radians(fov)));
    float r_z_delta = (float)((double) z_far / (double)(z_far - z_near));

    m[0].x = r_fov * ((float) height / (float) width);
    m[0].y = m[0].z = m[0].w = 0;

    m[1].y = r_fov;
    m[1].x = m[1].z = m[1].w = 0;

    m[2].z = r_z_delta;
    m[2].w = -1;
    m[2].x = m[2].y = 0;

    m[3].z = r_z_delta * z_near;
    m[3].x = m[3].y = m[3].w = 0;
}

extern inline __attribute((always_inline)) void look_at_lh(mat4_t dst, vec3_t cam_pos, vec3_t cam_look, vec3_t up_dir)
{
    const vec3_t look_dir = VEC3_NORMALIZE(cam_look.gen - cam_pos.gen);
    const vec3_t right_vec = VEC3_NORMALIZE(cross(up_dir, look_dir).gen);
    const vec3_t up = cross(look_dir, right_vec);

    dst[0].x = right_vec.x;
    dst[0].y = up.x;
    dst[0].z = look_dir.x;
    dst[0].w = 0;

    dst[1].x = right_vec.y;
    dst[1].y = up.y;
    dst[1].z = look_dir.y;
    dst[1].w = 0;

    dst[2].x = right_vec.z;
    dst[2].y = up.z;
    dst[2].z = look_dir.z;
    dst[2].w = 0;

    dst[3].x = -VEC3_DOT(right_vec.gen, cam_pos.gen);
    dst[3].y = -VEC3_DOT(up.gen, cam_pos.gen);
    dst[3].z = -VEC3_DOT(look_dir.gen, cam_pos.gen);
    dst[3].w = 1;
}

extern inline __attribute((always_inline)) void nlook_at_lh(mat4_t dst, vec3_t cam_pos, vec3_t cam_look, vec3_t up_dir)
{
    const vec3_t look_dir = VEC3_NORMALIZE(cam_look.gen - cam_pos.gen);
    const vec3_t right_vec = VEC3_NORMALIZE(cross(up_dir, look_dir).gen);
    const vec3_t up = cross(look_dir, right_vec);

    dst[0].x = right_vec.x;
    dst[0].y = up.x;
    dst[0].z = look_dir.x;

    dst[1].x = right_vec.y;
    dst[1].y = up.y;
    dst[1].z = look_dir.y;

    dst[2].x = right_vec.z;
    dst[2].y = up.z;
    dst[2].z = look_dir.z;

    dst[3].x = -VEC3_DOT(right_vec.gen, cam_pos.gen);
    dst[3].y = -VEC3_DOT(up.gen, cam_pos.gen);
    dst[3].z = -VEC3_DOT(look_dir.gen, cam_pos.gen);
}

extern inline __attribute((always_inline)) void look_at_rh(mat4_t dst, vec3_t cam_pos, vec3_t cam_look, vec3_t up_dir)
{
    const vec3_t look_dir = VEC3_NORMALIZE(cam_look.gen - cam_pos.gen);
    const vec3_t right_vec = VEC3_NORMALIZE(cross(look_dir, up_dir).gen);
    const vec3_t up = cross(right_vec, look_dir);

    dst[0].x = right_vec.x;
    dst[0].y = up.x;
    dst[0].z = -look_dir.x;
    dst[0].w = 0;

    dst[1].x = right_vec.y;
    dst[1].y = up.y;
    dst[1].z = -look_dir.y;
    dst[1].w = 0;

    dst[2].x = right_vec.z;
    dst[2].y = up.z;
    dst[2].z = -look_dir.z;
    dst[2].w = 0;

    dst[3].x = -VEC3_DOT(right_vec.gen, cam_pos.gen);
    dst[3].y = -VEC3_DOT(up.gen, cam_pos.gen);
    dst[3].z = VEC3_DOT(look_dir.gen, cam_pos.gen);
    dst[3].w = 1;
}

extern inline __attribute((always_inline)) void nlook_at_rh(mat4_t dst, vec3_t cam_pos, vec3_t cam_look, vec3_t up_dir)
{
    const vec3_t look_dir = VEC3_NORMALIZE(cam_look.gen - cam_pos.gen);
    const vec3_t right_vec = VEC3_NORMALIZE(cross(look_dir, up_dir).gen);
    const vec3_t up = cross(right_vec, look_dir);

    dst[0].x = right_vec.x;
    dst[0].y = up.x;
    dst[0].z = -look_dir.x;

    dst[1].x = right_vec.y;
    dst[1].y = up.y;
    dst[1].z = -look_dir.y;

    dst[2].x = right_vec.z;
    dst[2].y = up.z;
    dst[2].z = -look_dir.z;

    dst[3].x = -VEC3_DOT(right_vec.gen, cam_pos.gen);
    dst[3].y = -VEC3_DOT(up.gen, cam_pos.gen);
    dst[3].z = VEC3_DOT(look_dir.gen, cam_pos.gen);
}

void vec3ta(vec3_t u, char *dst)
{
    snprintf(dst, 64, "{%f, %f, %f}", u.x, u.y, u.z);
}

void vec4ta(vec4_t u, char *dst)
{
    snprintf(dst, 100, "{%f, %f, %f, %f}", u.x, u.y, u.z, u.z);
}

void mat4ta(mat4_t m, char *dst)
{
    snprintf(dst, 400, "{%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n%f %f %f %f}",
             m[0].x, m[1].x, m[2].x, m[3].x,
             m[0].y, m[1].y, m[2].y, m[3].y,
             m[0].z, m[1].z, m[2].z, m[3].z,
             m[0].w, m[1].w, m[2].w, m[3].w);
}

vec3_t mix3(vec3_t u, vec3_t v, vec3_t x)
{
    // u (1 - x) + vx <=> vx - (ux - u)
#if CONF >= FMA
    return v3C(_mm_fmsub_ps(v.gen, x.gen, _mm_fmsub_ps(u.gen, x.gen, u.gen)));
#endif
    // never use slow fma()
    return v3C(v.gen * x.gen - (u.gen * x.gen - u.gen));
}

vec4_t mix4(vec4_t u, vec4_t v, vec4_t x)
{
    // u (1 - x) + vx <=> vx - (ux - u)
#if CONF >= FMA
    return v4C(_mm_fmsub_ps(v.gen, x.gen, _mm_fmsub_ps(u.gen, x.gen, u.gen)));
#endif
    // never use slow fma()
    return v4C(v.gen * x.gen - (u.gen * x.gen - u.gen));
}