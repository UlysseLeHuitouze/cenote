//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
#   include <windows.h>
#endif
#include "cebootstrap.h"
#include <stdlib.h>
#include <string.h>

// length of "VK_" substring
#define VK_PREFIX_LENGTH 3
#define INLINE inline __attribute((always_inline))


VkDebugUtilsMessageSeverityFlagsEXT displayedDebugSeverities = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
                                                               VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                                               VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;



void cemakeswapchain(
        VkPhysicalDevice physicalDevice,
        VkDevice device,
        VkSwapchainCreateInfoKHR *swapchainCreateInfo,
        const VkAllocationCallbacks *allocator,
        VkImage *swapchainImages)
{

    VkSurfaceCapabilitiesKHR caps = {};
    CHECK_VK((vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, swapchainCreateInfo->surface, &caps)));

    VkExtent2D extent2D = caps.currentExtent.width == -1 ? (VkExtent2D){
            .width = CLAMP(caps.minImageExtent.width, caps.maxImageExtent.width, swapchainCreateInfo->imageExtent.width),
            .height = CLAMP(caps.minImageExtent.height, caps.maxImageExtent.height, swapchainCreateInfo->imageExtent.height)
    } : caps.currentExtent;

    swapchainCreateInfo->imageExtent.width = extent2D.width;
    swapchainCreateInfo->imageExtent.height = extent2D.height;

    if (swapchainCreateInfo->preTransform == 0)
        swapchainCreateInfo->preTransform = caps.currentTransform;

    VkSwapchainKHR swapchain = VK_NULL_HANDLE;

    CHECK_VK(vkCreateSwapchainKHR(device, swapchainCreateInfo, allocator, &swapchain));

    vkDestroySwapchainKHR(device, swapchainCreateInfo->oldSwapchain, allocator);

    swapchainCreateInfo->oldSwapchain = swapchain;

    uint32_t count = 0;
    CHECK_VK(vkGetSwapchainImagesKHR(device, swapchain, &count, VK_NULL_HANDLE));
#if DEBUG
    BEXIT(count != swapchainCreateInfo->minImageCount, "incoherent swapchain image count");
#endif
    CHECK_VK(vkGetSwapchainImagesKHR(device, swapchain, &count, swapchainImages));
}


static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallBack(
        VkDebugUtilsMessageSeverityFlagBitsEXT msgSeverity,
        VkDebugUtilsMessageTypeFlagsEXT msgType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        __attribute__((unused)) void *pUserData)
{
    if (displayedDebugSeverities & msgSeverity)
    {
        const char *msg = pCallbackData->pMessage;
        const size_t msg_len = strlen(msg);

        char *severity;
        size_t severity_len = 0;
        switch (msgSeverity)
        {
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
                severity = "[VERBOSE]";
                severity_len = 9;
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
                severity = "[INFO]";
                severity_len = 6;
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
                severity = "[WARNING]";
                severity_len = 9;
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
                severity = "[ERROR]";
                severity_len = 7;
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT:
                severity = "[MAX]";
                severity_len = 5;
                break;
            default:
                break;
        }

        char *type;
        size_t type_len = 0;
        switch (msgType)
        {
            case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
                type = "GENERAL";
                type_len = 7;
                break;
            case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
                type = "VALIDATION";
                type_len = 10;
                break;
            case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
                type = "PERFORMANCE";
                type_len = 11;
                break;
            default:
                break;
        }

        // length for "Validation layer | ", " msg type: " & " details: " & '\0'
        size_t extra_len = 19 + 11 + 10 + 1;

        char *final_text = smalloc(sizeof *final_text * (msg_len + severity_len + type_len + extra_len));


        size_t i = 0;

        final_text[i++] = 'V';final_text[i++] = 'a';final_text[i++] = 'l';final_text[i++] = 'i';final_text[i++] = 'd';
        final_text[i++] = 'a';final_text[i++] = 't';final_text[i++] = 'i';final_text[i++] = 'o';final_text[i++] = 'n';
        final_text[i++] = ' ';final_text[i++] = 'l';final_text[i++] = 'a';final_text[i++] = 'y';final_text[i++] = 'e';
        final_text[i++] = 'r';final_text[i++] = ' ';final_text[i++] = '|';final_text[i++] = ' ';

        for (uint32_t j = 0; j < severity_len; j++)
            final_text[i++] = severity[j];

        final_text[i++] = ' ';final_text[i++] = 'm';final_text[i++] = 's';final_text[i++] = 'g';
        final_text[i++] = ' ';final_text[i++] = 't';final_text[i++] = 'y';final_text[i++] = 'p';final_text[i++] = 'e';
        final_text[i++] = ':';final_text[i++] = ' ';

        for (uint32_t j = 0; j < type_len; j++)
            final_text[i++] = type[j];

        final_text[i++] = ' ';final_text[i++] = 'd';final_text[i++] = 'e';final_text[i++] = 't';
        final_text[i++] = 'a';final_text[i++] = 'i';final_text[i++] = 'l';final_text[i++] = 's';
        final_text[i++] = ':';final_text[i++] = ' ';

        for (uint32_t j = 0; j < msg_len; j++)
            final_text[i++] = msg[j];

        final_text[i] = '\0';

        switch (msgSeverity)
        {
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT:
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
                fprintf(stderr, "%s\n", final_text);
                break;
            default:
                printf("%s\n", final_text);
                break;
        }

        free(final_text);
    }
    return VK_FALSE;
}


static INLINE void primeSwapChain(VulkanSetupInput *input, VulkanSetupOutput *output) {

    VkSurfaceKHR surface = output->swapchainInfo->surface;
    VkPhysicalDevice physicalDevice = *output->physicalDevice;


    VkSurfaceCapabilitiesKHR *caps = smalloc(sizeof *caps);
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, caps);

    uint32_t formatCount = 0;
    uint32_t presentModeCount = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, VK_NULL_HANDLE);
    vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, VK_NULL_HANDLE);

    BEXIT(formatCount == 0 || presentModeCount == 0, "Selected GPU doesn't support vulkan swapchain");

    VkSurfaceFormatKHR *formats = smalloc(sizeof *formats * formatCount );
    VkPresentModeKHR *presentModes = smalloc(sizeof *presentModes * presentModeCount);

    vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, formats);
    vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, presentModes);

    // surface format filtering
    {
        output->swapchainInfo->imageFormat = formats[0].format;
        output->swapchainInfo->imageColorSpace = formats[0].colorSpace;
        bool found = false;
        for (uint32_t i = 0; !found && i < formatCount; i++) {
            VkSurfaceFormatKHR format = formats[i];
            if (format.format == input->requestedSwapchainInfo->surfaceFormat.format &&
                format.colorSpace == input->requestedSwapchainInfo->surfaceFormat.colorSpace)
            {
                output->swapchainInfo->imageFormat = format.format;
                output->swapchainInfo->imageColorSpace = format.colorSpace;
                found = true;
                break;
            }
        }

        if (!found)
            CA_WARN("Swapchain does not offer requested image format %d and color space %d",
                    input->requestedSwapchainInfo->surfaceFormat.format,
                    input->requestedSwapchainInfo->surfaceFormat.colorSpace);
    }

    // present mode filtering
    output->swapchainInfo->presentMode = VK_PRESENT_MODE_FIFO_KHR;
    for (uint32_t i = 0; i < presentModeCount; i++)
        if (presentModes[i] == input->requestedSwapchainInfo->presentMode) {
            output->swapchainInfo->presentMode = input->requestedSwapchainInfo->presentMode;
            break;
        }

    // image count choice
    {
        const uint32_t preferredImageCount = input->requestedSwapchainInfo->imageCount;
        output->swapchainInfo->minImageCount = caps->maxImageCount == 0 ? MAX(caps->minImageCount, preferredImageCount) :
                                             CLAMP(caps->minImageCount, caps->maxImageCount, preferredImageCount);
    }

    // 2D extent
    if (caps->currentExtent.width != -1)
        output->swapchainInfo->imageExtent = caps->currentExtent;
    else {
        output->swapchainInfo->imageExtent.width = CLAMP(caps->minImageExtent.width, caps->maxImageExtent.width,
                                                         output->swapchainInfo->imageExtent.width);
        output->swapchainInfo->imageExtent.height = CLAMP(caps->minImageExtent.height, caps->maxImageExtent.height,
                                                          output->swapchainInfo->imageExtent.height);
    }

    if ( output->swapchainInfo->preTransform == 0)
        output->swapchainInfo->preTransform = caps->currentTransform;

    free(presentModes);
    free(formats);
    free(caps);
}


static INLINE void createVmaAllocator(VulkanSetupInput *input, VulkanSetupOutput *output)
{
    VmaVulkanFunctions functions = {
            .vkGetInstanceProcAddr = vkGetInstanceProcAddr,
            .vkGetDeviceProcAddr = vkGetDeviceProcAddr,
            .vkGetPhysicalDeviceProperties = vkGetPhysicalDeviceProperties,
            .vkGetPhysicalDeviceMemoryProperties = vkGetPhysicalDeviceMemoryProperties,
            .vkAllocateMemory = vkAllocateMemory,
            .vkFreeMemory = vkFreeMemory,
            .vkMapMemory = vkMapMemory,
            .vkUnmapMemory = vkUnmapMemory,
            .vkFlushMappedMemoryRanges = vkFlushMappedMemoryRanges,
            .vkInvalidateMappedMemoryRanges = vkInvalidateMappedMemoryRanges,
            .vkBindBufferMemory = vkBindBufferMemory,
            .vkBindImageMemory = vkBindImageMemory,
            .vkGetBufferMemoryRequirements = vkGetBufferMemoryRequirements,
            .vkGetImageMemoryRequirements = vkGetImageMemoryRequirements,
            .vkCreateBuffer = vkCreateBuffer,
            .vkDestroyBuffer = vkDestroyBuffer,
            .vkCreateImage = vkCreateImage,
            .vkDestroyImage = vkDestroyImage,
            .vkCmdCopyBuffer = vkCmdCopyBuffer,
#if VK_VERSION_1_1
            .vkGetBufferMemoryRequirements2KHR = vkGetBufferMemoryRequirements2,
            .vkGetImageMemoryRequirements2KHR = vkGetImageMemoryRequirements2,
#elif VK_KHR_get_memory_requirements2
            .vkGetBufferMemoryRequirements2KHR = vkGetBufferMemoryRequirements2KHR,
            .vkGetImageMemoryRequirements2KHR = vkGetImageMemoryRequirements2KHR,
#endif
#if VK_VERSION_1_1
            .vkBindBufferMemory2KHR = vkBindBufferMemory2,
            .vkBindImageMemory2KHR = vkBindImageMemory2,
#elif VK_KHR_bind_memory2
            .vkBindBufferMemory2KHR = vkBindBufferMemory2KHR,
            .vkBindImageMemory2KHR = vkBindImageMemory2KHR,
#endif
#if VK_VERSION_1_1
            .vkGetPhysicalDeviceMemoryProperties2KHR = vkGetPhysicalDeviceMemoryProperties2,
#elif VK_KHR_get_physical_device_properties2
            .vkGetPhysicalDeviceMemoryProperties2KHR = vkGetPhysicalDeviceMemoryProperties2KHR,
#endif
#if VK_VERSION_1_3
            .vkGetDeviceBufferMemoryRequirements = vkGetDeviceBufferMemoryRequirements,
            .vkGetDeviceImageMemoryRequirements = vkGetDeviceImageMemoryRequirements
#elif
            .vkGetDeviceBufferMemoryRequirements = vkGetDeviceBufferMemoryRequirementsKHR,
            .vkGetDeviceImageMemoryRequirements = vkGetDeviceImageMemoryRequirementsKHR
#endif
    };
    input->requestedAllocatorInfo->pVulkanFunctions = &functions;
    SCHECK_VK(vmaCreateAllocator(input->requestedAllocatorInfo, output->allocator));
}


static bool arraySomeMinusOne(const uint32_t *arr, uint32_t length)
{
    for (int i = 0; i < length; i++)
        if (arr[i] == -1)
            return true;
    return false;
}

static INLINE void handleQueues(VulkanSetupInput *input, VulkanSetupOutput *output) {

    VkPhysicalDevice physicalDevice = *output->physicalDevice;

    uint32_t propCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &propCount, VK_NULL_HANDLE);

    VkQueueFamilyProperties *props = smalloc(sizeof *props * propCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &propCount, props);


    const uint32_t requestedQueueFamilyCount = input->requestedQueuesInfo->queueFamiliesFallback.len;
    output->queueFeatures->capacity = requestedQueueFamilyCount;
    for (int i = 0; i < requestedQueueFamilyCount; i++)
        output->queueFeatures->queueFamilyIndices[i] = -1;


    for (
            int i = 0;
            i < propCount && arraySomeMinusOne(output->queueFeatures->queueFamilyIndices, requestedQueueFamilyCount);
            i++) {

        VkBool32 surfaceSupported = VK_FALSE;
        VkQueueFamilyProperties prop = props[i];
        vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, output->swapchainInfo->surface, &surfaceSupported);

        for (int j = 0; j < requestedQueueFamilyCount; j++) {
            if (
                    output->queueFeatures->queueFamilyIndices[j] == -1 &&
                    input->requestedQueuesInfo->queueFamiliesSelectPredicate[j](prop, surfaceSupported)
                    )
                output->queueFeatures->queueFamilyIndices[j] = i;
        }
    }

    // fall back possibly once then give up
    const uint32_t *queueFamilyFallback = input->requestedQueuesInfo->queueFamiliesFallback.elements;
    for (int i = 0; i < requestedQueueFamilyCount; i++) {
        if (output->queueFeatures->queueFamilyIndices[i] == -1) {
            CA_WARN("GPU does not have a queue family fitting for predicate #%d. Falling back...", i);

            uint32_t f_idx = output->queueFeatures->queueFamilyIndices[queueFamilyFallback[i]];
            if (f_idx == -1) {
                CA_WARN("GPU does not have a queue family fitting for predicate #%d. Fall back failed!",
                        queueFamilyFallback[i]);
                f_idx = 0;
            }
            output->queueFeatures->queueFamilyIndices[i] = f_idx;
        }
    }

    free(props);
}


static INLINE void createDevice(VulkanSetupInput *input, VulkanSetupOutput *output) {
    IFDEBUG(CA_INFO("Creating VK device..."))

    handleQueues(input, output);

    VkPhysicalDevice physicalDevice = *output->physicalDevice;

    const uint32_t familyCount = input->requestedQueuesInfo->queueFamiliesFallback.len;
    // retrieve unique queue family indices
    uint32_t unique_family_count;
    uint32_t *unique_family_indices;
    uint32_t *queue_count_per_unique_family;
    // stores queue count per unique family by unique family index
    BoundIntIntMap map = ca_bound_intintmap_dynamic_of(familyCount, scalloc);
    {

        for (int i = 0 ; i < familyCount; i++) {
            uint32_t idx = output->queueFeatures->queueFamilyIndices[i];
            uint32_t old_queue_count = caiimapget(&map, idx);
            caiimapput(&map, idx, old_queue_count + 1 + (old_queue_count == UINT32_MAX));
        }

        unique_family_count = map.count;
        unique_family_indices = smalloc(sizeof(uint32_t) * 2 * unique_family_count);
        queue_count_per_unique_family = unique_family_indices + unique_family_count;
        caiimaparr(&map, unique_family_indices, queue_count_per_unique_family);

    }



    uint32_t queue_count_per_unique_family_max = 0;
    for (int i = 0; i < unique_family_count; i++)
        if (queue_count_per_unique_family[i] > queue_count_per_unique_family_max)
            queue_count_per_unique_family_max = queue_count_per_unique_family[i];


    float *priorities = smalloc(sizeof *priorities * queue_count_per_unique_family_max);
    for (int i = 0; i < queue_count_per_unique_family_max; i++)
        priorities[i] = 1;

    VkDeviceQueueCreateInfo *queueCreateInfo = ncalloc(unique_family_count, sizeof(VkDeviceQueueCreateInfo));

    for (uint32_t i = 0; i < unique_family_count; i++) {
        queueCreateInfo[i].sTypeDefault(VkDeviceQueueCreateInfo);
        queueCreateInfo[i].queueFamilyIndex = unique_family_indices[i];
        queueCreateInfo[i].queueCount = queue_count_per_unique_family[i];
        queueCreateInfo[i].pQueuePriorities = priorities;
    }

    VkDeviceCreateInfo deviceCreateInfo = {
            .sTypeDefault(VkDeviceCreateInfo),
            .pNext = input->pNextDeviceCreateInfo,
            .pQueueCreateInfos = queueCreateInfo,
            .queueCreateInfoCount = unique_family_count,
            .pEnabledFeatures = input->requestedDeviceFeatures,
            .ppEnabledExtensionNames = input->requestedDeviceExtensions.elements,
            .enabledExtensionCount = input->requestedDeviceExtensions.len,
            .ppEnabledLayerNames = input->requestedLayers.elements,
            .enabledLayerCount = input->requestedLayers.len
    };

    SCHECK_VK(vkCreateDevice(physicalDevice, &deviceCreateInfo, input->allocCallbacks, output->device));


    for (uint32_t i = 0; i < output->queueFeatures->capacity; i++) {
        uint32_t idx = output->queueFeatures->queueFamilyIndices[i];
        // used to index the queue in its family in a stack-like manner
        uint32_t queue_count = caiimapget(&map, idx);
        BEXIT(queue_count == UINT32_MAX || queue_count == 0,
              "Illegal state: queue count found to be zero for family %d", idx);
        caiimapput(&map, idx, queue_count - 1);

        vkGetDeviceQueue(
                *output->device,
                idx,
                queue_count - 1,
                output->queueFeatures->queues + i);
    }
    free(priorities);
    free(unique_family_indices);
    free(queueCreateInfo);
    ca_bound_intintmap_free(map, free);
}


static INLINE void validatePhysicalDevice(VulkanSetupInput *input, VulkanSetupOutput *output)
{
    IFDEBUG(CA_INFO("Validating selected VK GPU..."))
    
    VkPhysicalDevice physicalDevice = *output->physicalDevice;
    
    VkPhysicalDeviceProperties props = {};
    vkGetPhysicalDeviceProperties(physicalDevice, &props);
#if DEBUG
    if (props.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
        CA_WARN("Selected GPU isn't a dedicated one");
#endif
    VkPhysicalDeviceFeatures availableFeatures = {};
    vkGetPhysicalDeviceFeatures(physicalDevice, &availableFeatures);


    BEXIT((input->requestedDeviceFeatures->robustBufferAccess && !availableFeatures.robustBufferAccess) ||
          (input->requestedDeviceFeatures->fullDrawIndexUint32 && !availableFeatures.fullDrawIndexUint32) ||
          (input->requestedDeviceFeatures->imageCubeArray && !availableFeatures.imageCubeArray) ||
          (input->requestedDeviceFeatures->independentBlend && !availableFeatures.independentBlend) ||
          (input->requestedDeviceFeatures->geometryShader && !availableFeatures.geometryShader) ||
          (input->requestedDeviceFeatures->tessellationShader && !availableFeatures.tessellationShader) ||
          (input->requestedDeviceFeatures->sampleRateShading && !availableFeatures.sampleRateShading) ||
          (input->requestedDeviceFeatures->dualSrcBlend && !availableFeatures.dualSrcBlend) ||
          (input->requestedDeviceFeatures->logicOp && !availableFeatures.logicOp) ||
          (input->requestedDeviceFeatures->multiDrawIndirect && !availableFeatures.multiDrawIndirect) ||
          (input->requestedDeviceFeatures->drawIndirectFirstInstance && !availableFeatures.drawIndirectFirstInstance) ||
          (input->requestedDeviceFeatures->depthClamp && !availableFeatures.depthClamp) ||
          (input->requestedDeviceFeatures->depthBiasClamp && !availableFeatures.depthBiasClamp) ||
          (input->requestedDeviceFeatures->fillModeNonSolid && !availableFeatures.fillModeNonSolid) ||
          (input->requestedDeviceFeatures->depthBounds && !availableFeatures.depthBounds) ||
          (input->requestedDeviceFeatures->wideLines && !availableFeatures.wideLines) ||
          (input->requestedDeviceFeatures->largePoints && !availableFeatures.largePoints) ||
          (input->requestedDeviceFeatures->alphaToOne && !availableFeatures.alphaToOne) ||
          (input->requestedDeviceFeatures->multiViewport && !availableFeatures.multiViewport) ||
          (input->requestedDeviceFeatures->samplerAnisotropy && !availableFeatures.samplerAnisotropy) ||
          (input->requestedDeviceFeatures->textureCompressionETC2 && !availableFeatures.textureCompressionETC2) ||
          (input->requestedDeviceFeatures->textureCompressionASTC_LDR && !availableFeatures.textureCompressionASTC_LDR) ||
          (input->requestedDeviceFeatures->textureCompressionBC && !availableFeatures.textureCompressionBC) ||
          (input->requestedDeviceFeatures->occlusionQueryPrecise && !availableFeatures.occlusionQueryPrecise) ||
          (input->requestedDeviceFeatures->pipelineStatisticsQuery && !availableFeatures.pipelineStatisticsQuery) ||
          (input->requestedDeviceFeatures->vertexPipelineStoresAndAtomics &&
                !availableFeatures.vertexPipelineStoresAndAtomics) ||
          (input->requestedDeviceFeatures->fragmentStoresAndAtomics && !availableFeatures.fragmentStoresAndAtomics) ||
          (input->requestedDeviceFeatures->shaderTessellationAndGeometryPointSize
           && !availableFeatures.shaderTessellationAndGeometryPointSize) ||
          (input->requestedDeviceFeatures->shaderImageGatherExtended && !availableFeatures.shaderImageGatherExtended) ||
          (input->requestedDeviceFeatures->shaderStorageImageExtendedFormats &&
           !availableFeatures.shaderStorageImageExtendedFormats) ||
          (input->requestedDeviceFeatures->shaderStorageImageMultisample &&
                    !availableFeatures.shaderStorageImageMultisample) ||
          (input->requestedDeviceFeatures->shaderStorageImageReadWithoutFormat &&
           !availableFeatures.shaderStorageImageReadWithoutFormat) ||
          (input->requestedDeviceFeatures->shaderStorageImageWriteWithoutFormat &&
           !availableFeatures.shaderStorageImageWriteWithoutFormat) ||
          (input->requestedDeviceFeatures->shaderUniformBufferArrayDynamicIndexing
           && !availableFeatures.shaderUniformBufferArrayDynamicIndexing) ||
          (input->requestedDeviceFeatures->shaderSampledImageArrayDynamicIndexing
           && !availableFeatures.shaderSampledImageArrayDynamicIndexing) ||
          (input->requestedDeviceFeatures->shaderStorageBufferArrayDynamicIndexing
           && !availableFeatures.shaderStorageBufferArrayDynamicIndexing) ||
          (input->requestedDeviceFeatures->shaderStorageImageArrayDynamicIndexing
           && !availableFeatures.shaderStorageImageArrayDynamicIndexing) ||
          (input->requestedDeviceFeatures->shaderClipDistance && !availableFeatures.shaderClipDistance) ||
          (input->requestedDeviceFeatures->shaderCullDistance && !availableFeatures.shaderCullDistance) ||
          (input->requestedDeviceFeatures->shaderFloat64 && !availableFeatures.shaderFloat64) ||
          (input->requestedDeviceFeatures->shaderInt64 && !availableFeatures.shaderInt64) ||
          (input->requestedDeviceFeatures->shaderInt16 && !availableFeatures.shaderInt16) ||
          (input->requestedDeviceFeatures->shaderResourceResidency && !availableFeatures.shaderResourceResidency) ||
          (input->requestedDeviceFeatures->shaderResourceMinLod && !availableFeatures.shaderResourceMinLod) ||
          (input->requestedDeviceFeatures->sparseBinding && !availableFeatures.sparseBinding) ||
          (input->requestedDeviceFeatures->sparseResidencyBuffer && !availableFeatures.sparseResidencyBuffer) ||
          (input->requestedDeviceFeatures->sparseResidencyImage2D && !availableFeatures.sparseResidencyImage2D) ||
          (input->requestedDeviceFeatures->sparseResidencyImage3D && !availableFeatures.sparseResidencyImage3D) ||
          (input->requestedDeviceFeatures->sparseResidency2Samples && !availableFeatures.sparseResidency2Samples) ||
          (input->requestedDeviceFeatures->sparseResidency4Samples && !availableFeatures.sparseResidency4Samples) ||
          (input->requestedDeviceFeatures->sparseResidency8Samples && !availableFeatures.sparseResidency8Samples) ||
          (input->requestedDeviceFeatures->sparseResidency16Samples && !availableFeatures.sparseResidency16Samples) ||
          (input->requestedDeviceFeatures->sparseResidencyAliased && !availableFeatures.sparseResidencyAliased) ||
          (input->requestedDeviceFeatures->variableMultisampleRate && !availableFeatures.variableMultisampleRate) ||
          (input->requestedDeviceFeatures->inheritedQueries && !availableFeatures.inheritedQueries)
            , "Selected device does not support some of the requested device features");



    uint32_t availableExtensionCount = 0;
    vkEnumerateDeviceExtensionProperties(physicalDevice, NULL, &availableExtensionCount, NULL);

    VkExtensionProperties *availableExtensionProps = smalloc(sizeof *availableExtensionProps * availableExtensionCount);
    vkEnumerateDeviceExtensionProperties(physicalDevice, NULL, &availableExtensionCount, availableExtensionProps);


    BoundHashSet availableExtensionNameSet = ca_bound_hashset_dynamic_of(
            availableExtensionCount,
            strcmp,
            ncalloc
    );

    for (uint32_t i = 0; i < availableExtensionCount; i++)
        cahsetput(
                &availableExtensionNameSet,
                castrhash_unsafe(VK_PREFIX_LENGTH, availableExtensionProps[i].extensionName),
                availableExtensionProps[i].extensionName
        );


    const uint32_t requestedDeviceExtensionCount = input->requestedDeviceExtensions.len;
    const char **requestedDeviceExtensions = input->requestedDeviceExtensions.elements;
    for (uint32_t i = 0; i < requestedDeviceExtensionCount; i++) {
        BEXIT(!cahsethas(
                      &availableExtensionNameSet,
                      castrhash_unsafe(VK_PREFIX_LENGTH, requestedDeviceExtensions[i]),
                      requestedDeviceExtensions[i]
              ),
              "%s device extension is not available for the selected device", requestedDeviceExtensions[i]);
    }


    ca_bound_hashset_free(availableExtensionNameSet, free);

    free(availableExtensionProps);
}

static INLINE void pickPhysicalDevice(VulkanSetupInput *input, VulkanSetupOutput *output)
{
    IFDEBUG(CA_INFO("Picking VK GPU..."))


    uint32_t physicalDeviceCount;
    VkResult res = vkEnumeratePhysicalDevices(*output->instance, &physicalDeviceCount, NULL);
    if (res == VK_INCOMPLETE)
        CA_WARN("vkEnumeratePhysicalDevices returned VK_INCOMPLETE. May have side-effects");
    else SCHECK_VK(res);
    
    BEXIT(physicalDeviceCount == 0, "No GPU supporting Vulkan was found");

    VkPhysicalDevice *devices = smalloc(sizeof(VkPhysicalDevice) * physicalDeviceCount);
    vkEnumeratePhysicalDevices(*output->instance, &physicalDeviceCount, devices);

    const char *GPUName = input->GPUName;
    bool found = 0;
    for (uint32_t i = 0; i < physicalDeviceCount && !found; i++) {
        VkPhysicalDeviceProperties props = {};
        vkGetPhysicalDeviceProperties(devices[i], &props);
        const char *gpuName = props.deviceName;
        if (strstr(gpuName, GPUName) != NULL) {
            CA_INFO("%s GPU was deemed suitable for requested GPU %s", gpuName, GPUName);
            *output->physicalDevice = devices[i];
            found = 1;
        }
    }
    BEXIT(found == 0, "Couldn't find a GPU named %s", GPUName);
    free(devices);
}


// don't bother declaring if not debug
#if DEBUG
static INLINE void createDebugMessenger(VulkanSetupInput *input, VulkanSetupOutput *output,
                          const VkDebugUtilsMessengerCreateInfoEXT *pDebugMessengerCreateInfo)
{
    CA_INFO("Creating VK debug messenger...");
    VkInstance instance = *output->instance;
    PFN_vkCreateDebugUtilsMessengerEXT func = (PFN_vkCreateDebugUtilsMessengerEXT)
            vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    BEXIT(
            func == VK_NULL_HANDLE ||
            func(instance, pDebugMessengerCreateInfo, input->allocCallbacks, output->debugMessenger) != VK_SUCCESS,
          "Debug Utils Messenger Extension does not exist. Cannot setup debug messenger.");
}
#endif //DEBUG


static INLINE void createInstance(VulkanSetupInput *input, VulkanSetupOutput *output, const void *pNext) {
    IFDEBUG(CA_INFO("Creating VK instance..."))


    // requested layer validation
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, NULL);

    VkLayerProperties *availableLayers = smalloc(sizeof *availableLayers * layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers);

    BoundHashSet availableLayerSet = ca_bound_hashset_dynamic_of(
            layerCount,
            strcmp,
            ncalloc
    );


    for (uint32_t i = 0; i < layerCount; i++) {
        cahsetput(
                &availableLayerSet,
                castrhash_unsafe(VK_PREFIX_LENGTH,availableLayers[i].layerName),
                availableLayers[i].layerName
        );
    }


    const uint32_t requestedLayerCount = input->requestedLayers.len;
    const char **requestedLayers = input->requestedLayers.elements;
    for (uint32_t i = 0; i < requestedLayerCount; i++) {
        BEXIT(!cahsethas(
                &availableLayerSet,
                castrhash_unsafe(VK_PREFIX_LENGTH, requestedLayers[i]),
                requestedLayers[i]
              ),
              "%s layer is not available", requestedLayers[i]
        );
    }


    ca_bound_hashset_free(availableLayerSet, free);
    free(availableLayers);


    // requested extensions validation
    uint32_t availableExtCount = 0;
    vkEnumerateInstanceExtensionProperties(NULL, &availableExtCount, NULL);

    VkExtensionProperties *availableExtensions = nmalloc(sizeof *availableExtensions * availableExtCount);
    vkEnumerateInstanceExtensionProperties(NULL, &availableExtCount, availableExtensions);

    BoundHashSet availableExtSet = ca_bound_hashset_dynamic_of(
            availableExtCount,
            strcmp,
            ncalloc
    );

    for (uint32_t i = 0; i < availableExtCount; i++) {
        cahsetput(&availableExtSet,
                  castrhash_unsafe(VK_PREFIX_LENGTH, availableExtensions[i].extensionName),
                  availableExtensions[i].extensionName
        );
    }


    const uint32_t requestedInstanceExtensionCount = input->requestedInstanceExtensions.len;
    const char **requestedInstanceExtensions = input->requestedInstanceExtensions.elements;
    for (uint32_t i = 0; i < requestedInstanceExtensionCount; i++) {
        BEXIT(!cahsethas(&availableExtSet,
                         castrhash_unsafe(VK_PREFIX_LENGTH, requestedInstanceExtensions[i]),
                         requestedInstanceExtensions[i]
              ),
              "%s layer is not available", requestedInstanceExtensions[i]
        );
    }


    ca_bound_hashset_free(availableExtSet, free);
    free(availableExtensions);


    VkInstanceCreateInfo createInfo = {
            .sTypeDefault(VkInstanceCreateInfo),
            .pNext = pNext,
            .pApplicationInfo = input->appInfo,
            .enabledLayerCount = requestedLayerCount,
            .ppEnabledLayerNames = requestedLayers,
            .enabledExtensionCount = requestedInstanceExtensionCount,
            .ppEnabledExtensionNames = requestedInstanceExtensions
    };

    SCHECK_VK(vkCreateInstance(&createInfo, input->allocCallbacks, output->instance));

}


void ceinitvk(VulkanSetupInput *input, VulkanSetupOutput *output) {

#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
    /*SetEnvironmentVariable("DISABLE_LAYER_AMD_SWITCHABLE_GRAPHICS_1", "1");
    SetEnvironmentVariable("DISABLE_LAYER_NV_OPTIMUS_1", "1");
    SetEnvironmentVariable("ENABLE_MIRILLIS_LAYER", "1");*/
    // see https://github.com/KhronosGroup/Vulkan-Loader/blob/main/docs/LoaderLayerInterface.md#layer-special-case-disable
    SetEnvironmentVariable("VK_LOADER_LAYERS_DISABLE", "~implicit~");   // also ~all~ or ~explicit~

#elif defined(__linux__) || defined(__APPLE__)
    setenv("VK_LOADER_LAYERS_DISABLE", "~implicit~", 1);   // also ~all~ or ~explicit~
#endif

#if DEBUG
    BEXIT(
            !input ||
            !input->requestedLayers.elements || !input->requestedInstanceExtensions.elements ||
            !input->requestedDeviceExtensions.elements||
            !input->appInfo || !input->GPUName ||
            !input->getInstanceExtensions_ptr || !input->createVkSurfaceKHR_ptr,
            "Missing some fields in a VulkanSetupInput");

    BEXIT(
            !output ||
            !output->instance || !output->debugMessenger || !output->physicalDevice || !output->device ||
            !output->queueFeatures || !output->swapchainInfo || !output->allocator,
            "Missing some fields in a VulkanSetupOutput");

    VkDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo = {
            .sTypeDefault(VkDebugUtilsMessengerCreateInfoEXT),
            .messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                               VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
                               VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                               VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
            .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                           VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                           VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
            .pfnUserCallback = debugCallBack
    };

    VkValidationFeaturesEXT validationFeaturesExt = {
            .sTypeDefault(VkValidationFeaturesEXT),
            .pNext = &debugMessengerCreateInfo,
            .enabledValidationFeatureCount = 1,
            .pEnabledValidationFeatures = ARRAY_OF(VkValidationFeatureEnableEXT,
                                                   VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT)
    };

    const void *pNextInstanceCreateInfo = &validationFeaturesExt;


    {
        uint32_t old_len = input->requestedLayers.len;
        uint32_t new_len = old_len + 1;
        const char **layers_ptr = smalloc(sizeof *layers_ptr * new_len);

        memcpy(layers_ptr, input->requestedLayers.elements, sizeof *layers_ptr * old_len);

        layers_ptr[old_len] = "VK_LAYER_KHRONOS_validation";
        input->requestedLayers.elements = layers_ptr;
        input->requestedLayers.len = new_len;
    }
#else
    const void *pNextInstanceCreateInfo = NULL;
#endif

    const char **extra_extensions;
    const bool has_extra_extensions = input->getInstanceExtensions_ptr;
    {
        uint32_t extra_len = 0;
        if (has_extra_extensions)
            SCHECK_VK(input->getInstanceExtensions_ptr(&extra_len, &extra_extensions));

        uint32_t old_len = input->requestedInstanceExtensions.len;
        uint32_t new_len = old_len + extra_len + DEBUG;
        const char **ext_ptr = smalloc(sizeof *ext_ptr * new_len);

        memcpy(ext_ptr, input->requestedInstanceExtensions.elements, sizeof *ext_ptr * old_len);
        if (has_extra_extensions)
            memcpy(ext_ptr + old_len, extra_extensions, sizeof *ext_ptr * extra_len);

        IFDEBUG(ext_ptr[new_len - 1] = VK_EXT_DEBUG_UTILS_EXTENSION_NAME)
        input->requestedInstanceExtensions.elements = ext_ptr;
        input->requestedInstanceExtensions.len = new_len;
    }

    SCHECK_VK(volkInitialize());


    input->requestedAllocatorInfo->vulkanApiVersion = input->appInfo->apiVersion;
    input->requestedAllocatorInfo->pAllocationCallbacks = input->allocCallbacks;

    createInstance(input, output, pNextInstanceCreateInfo);
    volkLoadInstance(*output->instance);
    if (has_extra_extensions && input->freeInstanceExtensions_ptr)
        input->freeInstanceExtensions_ptr(extra_extensions);
    input->requestedAllocatorInfo->instance = *output->instance;

#if DEBUG
    createDebugMessenger(input, output, &debugMessengerCreateInfo);
#else
    *output->debugMessenger = VK_NULL_HANDLE;
#endif

    pickPhysicalDevice(input, output);
    validatePhysicalDevice(input, output);
    input->requestedAllocatorInfo->physicalDevice = *output->physicalDevice;

    SCHECK_VK(input->createVkSurfaceKHR_ptr(*output->instance, input->allocCallbacks, &output->swapchainInfo->surface));

    createDevice(input, output);
    volkLoadDevice(*output->device);
    input->requestedAllocatorInfo->device = *output->device;

    createVmaAllocator(input, output);
    primeSwapChain(input, output);


    IFDEBUG(free(input->requestedLayers.elements))
    free(input->requestedInstanceExtensions.elements);
}

void cedestroyvk(
        VkInstance vkInstance,
        const VkAllocationCallbacks *allocator,
        VkDebugUtilsMessengerEXT debugMessenger,
        VkSurfaceKHR surface,
        VkDevice device) {
    vkDestroyDevice(device, allocator);
    vkDestroySurfaceKHR(vkInstance, surface, allocator);
#if DEBUG
    PFN_vkDestroyDebugUtilsMessengerEXT fun = (PFN_vkDestroyDebugUtilsMessengerEXT)
            vkGetInstanceProcAddr(vkInstance, "vkDestroyDebugUtilsMessengerEXT");
    if (fun != NULL)
        fun(vkInstance, debugMessenger, allocator);
#endif
    vkDestroyInstance(vkInstance, allocator);
}