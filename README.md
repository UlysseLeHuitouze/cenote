# cenote

## Abstract

A [Vulkan](https://www.vulkan.org/), toy library for my toy projects.

Written in GNU-C17 (extensions used are mainly Statement Expressions, Function Attributes, Vector Extension and some built-in functions).

Features:

- Vulkan bootstrap abstraction {`cebootstrap.h`}
- Vma allocations abstraction {`cetypes.h`}
- Lookup and projection matrices abstraction {`cemaths.h`}
- Single-instance, single-device, single-surface vulkan application abstraction {`cenote.h`}
- Staging abstraction {`cetransfer_services.h`}

Work in progress:

- Optional concise aliases for Vulkan functions
- Batch queue submits in the staging abstraction

## Usage

A few settings can be customized using macro before including the headers. Thus, this library is meant to be copy-pasted in every project for now. I may put a STB-like single header after a few revamps.

### Dependencies

The following dependencies are required to use cenote:

- [Volk.h](https://github.com/zeux/volk) (Available with the [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/))
- [vk_mem_alloc.h](https://gpuopen.com/vulkan-memory-allocator/) (Available with the [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/))
- [stb_image.h](https://github.com/nothings/stb)
- [carig.h](https://gitlab.com/UlysseLeHuitouze/carig)
