//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CENOTE_H
#define CENOTE_H

#include "ceextern.h"
#include "cefunctions.h"
#include "cebootstrap.h"
#include "cetypes.h"
#include "cetransfer_services.h"


#ifndef CE_FRAME_COUNT
#   define CE_FRAME_COUNT 3U
#endif //CE_FRAME_COUNT


/**
 * See cetypes.h's VulkanSetupInput struct documentation
 */
typedef struct CenoteCreateInfo {
    char *appName;
    uint32_t appVersion;
    uint32_t apiVersion;
    uint32_t windowWidth;
    uint32_t windowHeight;
    array requestedLayers;
    array requestedInstanceExtensions;
    array requestedDeviceExtensions;
    void *pNextDeviceCreateInfo;
    char *GPUName;
    VkResult (*getInstanceExtensions_ptr)(uint32_t*, const char***);
    VkResult (*createVkSurfaceKHR_ptr)(VkInstance, const VkAllocationCallbacks*, VkSurfaceKHR*);
    VkPhysicalDeviceFeatures *requestedDeviceFeatures;
    QueueFamilySelectInfo *requestedQueuesInfo;
    SwapchainSetupInfo *requestedSwapchainInfo;
    VmaAllocatorCreateInfo *requestedAllocatorInfo;
} CenoteCreateInfo;

/**
 * Starts cenote engine, initializing Vulkan.
 * @param input
 */
void cenote_use(CenoteCreateInfo *createInfo, QueueFeatures *outputQueues);
/**
 * Close cenote engine, cleaning up Vulkan.
 */
void cenote_close();
/**
 * Recreates the swapchain and framebuffers' images for a resize.
 * <ul>
 *  <li>Must not be called before render pass are set, using #cenote_set_render_pass.</li>
 *  <li>Must not be called before framebuffers' attachments are set, using #cenote_set_framebuffer_attachments.</li>
 * </ul>
 * @param width the width after resizing
 * @param height the height after resizing
 */
void cenote_on_resize(uint32_t width, uint32_t height);

// getters

// all getters are inlined.

VkInstance cenote_get_instance();
VkDebugUtilsMessengerEXT cenote_get_debug_messenger();
VkPhysicalDevice cenote_get_physical_device();
VkDevice cenote_get_device();
/**
 * @return cenote's swapchain create info. Intended read-only, do not modify!
 */
VkSwapchainCreateInfoKHR* cenote_get_swapchain_info();
VkSwapchainKHR cenote_get_swapchain();
VkImage cenote_get_swapchain_image(uint32_t index);
VkImageView cenote_get_swapchain_image_view(uint32_t index);
VkFramebuffer cenote_get_framebuffer(uint32_t index);
VkRenderPass cenote_get_render_pass();
uint32_t cenote_get_swapchain_width();
uint32_t cenote_get_swapchain_height();
VmaAllocator cenote_get_allocator();
VkSurfaceKHR cenote_get_surface();
VkQueue cenote_get_main_queue();
VkQueue cenote_get_secondary_queue();
uint32_t cenote_get_queue_family_index();


// setters

// setters are used to allow step-by-step initialization past the boilerplate VkInstance, VkDevice, VkSurfaceKHR stuff.
// despite that, they are fully part of cenote and will be destroyed automatically upon calling #cenote_close.


/**
 * Cenote uses a single render pass architecture.
 * Sets cenote's one render pass.
 * Will be destroyed upon calling #cenote_close.
 * Do not destroy it yourself.
 * @param renderPass the main render pass
 */
void cenote_set_render_pass(VkRenderPass renderPass);


/**
 * Cenote uses a single framebuffer per frame (due to having only 1 render pass).
 * Sets cenote's framebuffers' attachment count. To set cenote's framebuffers attachments afterwards, use
 * #cenote_set_framebuffers_attachments.<br>
 * #cenote_set_framebuffers_attachments <code>pAttachments</code> length must be equal to <code>attachmentCount</code>
 * of the last call to this function.
 * <br><br>
 * The reason for the two fields to be separated in two functions is that setting <code>attachmentCount</code>
 * triggers a malloc whereas just setting </code>pAttachments</code> does not (assuming correct length is passed).
 * <br><br>
 * Typically, an application will call #cenote_set_framebuffers_attachment_count at the very beginning to pre-allocate
 * attachments, then the application will call #cenote_set_framebuffers_attachments after each resize.
 *
 * <ul>
 *  <li>Call this function to set the length of future framebuffer' attachments.</li>
 *  <li>Not calling this function or passing erroneous <code>attachmentCount</code> will most likely result in a SEGFAULT
 *   upon calling #cenote_set_framebuffers_attachments.</li>
 * </ul>
 * @param attachmentCount #cenote_set_framebuffers_attachments' <code>pAttachments</code> length
 * @see #cenote_set_framebuffers_attachments
 */
void cenote_set_framebuffers_attachment_count(uint32_t attachmentCount);


/**
 * Cenote uses a single framebuffer per frame (due to having only 1 render pass).<br>
 * Sets cenote's framebuffers' attachments such as depth attachment.<br>
 * <code>pAttachments</code> length must match with <code>attachmentCount</code> of the last call to
 * #cenote_set_framebuffers_attachment_count.
 * <br><br>
 * See #cenote_set_framebuffers_attachment_count doc as for why it is designed that way.
 * <br><br>
 * <ul>
 *  <li>Only swapchain image views attachment changes from one frame in flight to another and will be set internally.</li>
 *  <li>Do not add the swapchain image views in <code>pAttachments</code>.</li>
 *  <li><code>pAttachments</code> will be destroyed upon calling #cenote_close or upon calling this function again.</li>
 *  <li>Do not destroy them yourself.</li>
 * </ul>
 * @param pAttachments the image view attachments for all framebuffers, excluding swapchain image views
 * @see #cenote_set_framebuffers_attachment_count
 */
void cenote_set_framebuffers_attachments(const VkImageView *pAttachments);



// cenote wrappers for CeBuffers & CeImages

#define ceCreateCeImage(pImageCreateInfo, pAllocationCreateInfo, pImage, pAllocationInfo) \
CHECK_VK(vmaCreateImage(cenote_get_allocator(), pImageCreateInfo, pAllocationCreateInfo,  \
            &(pImage)->vkImage, &(pImage)->vmaAllocation, pAllocationInfo))


#define ceDestroyCeImage(image) vmaDestroyImage(cenote_get_allocator(), image.vkImage, image.vmaAllocation)

#define ceCreateCeBuffer(pBufferCreateInfo, pAllocationCreateInfo, pBuffer, pAllocationInfo) \
CHECK_VK(vmaCreateBuffer(cenote_get_allocator(), pBufferCreateInfo, pAllocationCreateInfo,   \
            &(pBuffer)->vkBuffer, &(pBuffer)->vmaAllocation, pAllocationInfo))

#define ceDestroyCeBuffer(buffer) vmaDestroyBuffer(cenote_get_allocator(), buffer.vkBuffer, buffer.vmaAllocation)


#define ceDestroyCeTexture(texture)              \
        ceDestroyImageView(texture.vkImageView); \
        ceDestroyCeImage(texture.ceImage)

#define ceDestroyCeSampledTexture(sampledTexture)   \
        ceDestroySampler(sampledTexture.vkSampler); \
        ceDestroyCeTexture(sampledTexture.ceTexture)




// Vulkan copy-pasta


/**
 * Returns properties of a physical device
 * @param pProperties is a pointer to a VkPhysicalDeviceProperties structure in which properties are returned.
 */
#define ceGetPhysicalDeviceProperties(pProperties) \
        vkGetPhysicalDeviceProperties(cenote_get_physical_device(), pProperties)


/**
 * Returns properties of a physical device
 * @param pProperties is a pointer to a VkPhysicalDeviceProperties2 structure in which properties are returned.
 */
#define ceGetPhysicalDeviceProperties2(pProperties) \
        vkGetPhysicalDeviceProperties2(cenote_get_physical_device(), pProperties)


/**
 * Creates an image view from an existing image
 * @param pCreateInfo is a pointer to a VkImageViewCreateInfo structure containing parameters to be used to create
 *                  the image view.
 * @param pView is a pointer to a VkImageView handle in which the resulting image view object is returned.
 */
#define ceCreateImageView(pCreateInfo, pView) \
        CHECK_VK(vkCreateImageView(cenote_get_device(), pCreateInfo, NULL, pView))


/**
 * Creates a new command pool object
 * @param pCreateInfo is a pointer to a VkCommandPoolCreateInfo structure specifying the state of the command pool object.
 * @param pCommandPool is a pointer to a VkCommandPool handle in which the created pool is returned.
 */
#define ceCreateCommandPool(pCreateInfo, pCommandPool) \
        CHECK_VK(vkCreateCommandPool(cenote_get_device(), pCreateInfo, NULL, pCommandPool))


/**
 * Allocates command buffers from an existing command pool
 * @param pAllocateInfo is a pointer to a VkCommandBufferAllocateInfo structure describing parameters of the allocation.
 * @param pCommandBuffers is a pointer to an array of VkCommandBuffer handles in
 * which the resulting command buffer objects are returned.
 * The array must be at least the length specified by the commandBufferCount member of pAllocateInfo.
 * Each allocated command buffer begins in the initial state.
 */
#define ceAllocateCommandBuffers(pAllocateInfo, pCommandBuffers) \
        CHECK_VK(vkAllocateCommandBuffers(cenote_get_device(), pAllocateInfo, pCommandBuffers))


/**
 * Creates a new fence object
 * @param pCreateInfo is a pointer to a VkFenceCreateInfo structure containing information about
 *                  how the fence is to be created.
 * @param pFence is a pointer to a handle in which the resulting fence object is returned.
 */
#define ceCreateFence(pCreateInfo, pFence) \
        CHECK_VK(vkCreateFence(cenote_get_device(), pCreateInfo, NULL, pFence))


/**
 * Creates a new queue semaphore object
 * @param pCreateInfo is a pointer to a VkSemaphoreCreateInfo structure containing information about
 *              how the semaphore is to be created.
 * @param pSemaphore is a pointer to a handle in which the resulting semaphore object is returned.
 */
#define ceCreateSemaphore(pCreateInfo, pSemaphore) \
        CHECK_VK(vkCreateSemaphore(cenote_get_device(), pCreateInfo, NULL, pSemaphore))


/**
 * Creates a new shader module object
 * @param pCreateInfo is a pointer to a VkShaderModuleCreateInfo structure.
 * @param pShaderModule is a pointer to a VkShaderModule handle in which the resulting shader module object is returned.
 */
#define ceCreateShaderModule(pCreateInfo, pShaderModule) \
        CHECK_VK(vkCreateShaderModule(cenote_get_device(), pCreateInfo, NULL, pShaderModule))


/**
 * Destroys a sampler object
 * @param sampler is the sampler to destroy.
 */
#define ceDestroySampler(sampler) \
        vkDestroySampler(cenote_get_device(), sampler, NULL)


/**
 * Destroys an image view object
 * @param imageView is the image view to destroy.
 */
#define ceDestroyImageView(imageView) \
        vkDestroyImageView(cenote_get_device(), imageView, NULL)


/**
 * Destroys an image object
 * @param image is the image to destroy.
 */
#define ceDestroyImage(image) \
        vkDestroyImage(cenote_get_device(), image, NULL)


/**
 * Inserts a memory dependency
 * @param commandBuffer is the command buffer into which the command is recorded.
 * @param srcStageMask is a bitmask of VkPipelineStageFlagBits specifying the source stages.
 * @param dstStageMask is a bitmask of VkPipelineStageFlagBits specifying the destination stages.
 * @param dependencyFlags is a bitmask of VkDependencyFlagBits specifying how execution and memory dependencies are formed.
 * @param memoryBarrierCount is the length of the pMemoryBarriers array.
 * @param pMemoryBarriers is a pointer to an array of VkMemoryBarrier structures.
 * @param bufferMemoryBarrierCount is the length of the pBufferMemoryBarriers array.
 * @param pBufferMemoryBarriers is a pointer to an array of VkBufferMemoryBarrier structures.
 * @param imageMemoryBarrierCount is the length of the pImageMemoryBarriers array.
 * @param pImageMemoryBarriers is a pointer to an array of VkImageMemoryBarrier structures.
 */
#define ceCmdPipelineBarrier(commandBuffer, srcStageMask, dstStageMask, dependencyFlags, \
memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers,    \
imageMemoryBarrierCount, pImageMemoryBarriers) \
        vkCmdPipelineBarrier(commandBuffer, srcStageMask, dstStageMask, dependencyFlags, \
        memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, \
        imageMemoryBarrierCount, pImageMemoryBarriers)


/**
 * Copies data from a buffer into an image
 * @param commandBuffer is the command buffer into which the command will be recorded.
 * @param srcBuffer is the source buffer.
 * @param dstImage is the destination image.
 * @param dstImageLayout is the layout of the destination image subresources for the copy.
 * @param regionCount is the number of regions to copy.
 * @param pRegions is a pointer to an array of VkBufferImageCopy structures specifying the regions to copy.
 */
#define ceCmdCopyBufferToImage(commandBuffer, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions) \
        vkCmdCopyBufferToImage(commandBuffer, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions)


/**
 * Creates a new render pass object
 * @param pCreateInfo is a pointer to a VkRenderPassCreateInfo structure describing the parameters of the render pass.
 * @param pRenderPass is a pointer to a VkRenderPass handle in which the resulting render pass object is returned.
 */
#define ceCreateRenderPass(pCreateInfo, pRenderPass) \
        CHECK_VK(vkCreateRenderPass(cenote_get_device(), pCreateInfo, NULL, pRenderPass))


/**
 * Creates a new framebuffer object
 * @param pCreateInfo is a pointer to a VkFramebufferCreateInfo structure describing additional information about
 *              framebuffer creation.
 * @param pFramebuffer is a pointer to a VkFramebuffer handle in which the resulting framebuffer object is returned.
 */
#define ceCreateFramebuffer(pCreateInfo, pFramebuffer) \
        CHECK_VK(vkCreateFramebuffer(cenote_get_device(), pCreateInfo, NULL, pFramebuffer))


/**
 * Creates a new descriptor set layout
 * @param pCreateInfo is a pointer to a VkDescriptorSetLayoutCreateInfo structure specifying the state
 * of the descriptor set layout object.
 * @param pSetLayout is a pointer to a VkDescriptorSetLayout handle in which the resulting descriptor set layout object
 *              is returned.
 */
#define ceCreateDescriptorSetLayout(pCreateInfo, pSetLayout) \
        CHECK_VK(vkCreateDescriptorSetLayout(cenote_get_device(), pCreateInfo, NULL, pSetLayout))


/**
 * Creates a new pipeline layout object
 * @param pCreateInfo is a pointer to a VkPipelineLayoutCreateInfo structure specifying the state of
 *              the pipeline layout object.
 * @param pPipelineLayout is a pointer to a VkPipelineLayout handle in which the resulting pipeline
 *              layout object is returned.
 */
#define ceCreatePipelineLayout(pCreateInfo, pPipelineLayout) \
        CHECK_VK(vkCreatePipelineLayout(cenote_get_device(), pCreateInfo, NULL, pPipelineLayout))


/**
 * Creates graphics pipelines
 * @param pipelineCache is either VK_NULL_HANDLE, indicating that pipeline caching is disabled;
 * or the handle of a valid pipeline cache object, in which case use of that cache is enabled for
 *              the duration of the command.
 * @param createInfoCount is the length of the pCreateInfos and pPipelines arrays.
 * @param pCreateInfos is a pointer to an array of VkGraphicsPipelineCreateInfo structures.
 * @param pPipelines is a pointer to an array of VkPipeline handles in which the resulting graphics pipeline objects
 *              are returned.
 */
#define ceCreateGraphicsPipelines(pipelineCache, createInfoCount, pCreateInfos, pPipelines) \
        CHECK_VK(vkCreateGraphicsPipelines(cenote_get_device(), pipelineCache, createInfoCount, \
        pCreateInfos, NULL, pPipelines))


/**
 * Destroys a shader module
 * @param shaderModule is the handle of the shader module to destroy.
 */
#define ceDestroyShaderModule(shaderModule) \
        vkDestroyShaderModule(cenote_get_device(), shaderModule, NULL)

/**
 * Creates a descriptor pool object
 * @param pCreateInfo is a pointer to a VkDescriptorPoolCreateInfo structure specifying the state of
 *              the descriptor pool object.
 * @param pDescriptorPool is a pointer to a VkDescriptorPool handle in which
 *              the resulting descriptor pool object is returned.
 */
#define ceCreateDescriptorPool(pCreateInfo, pDescriptorPool) \
        CHECK_VK(vkCreateDescriptorPool(cenote_get_device(), pCreateInfo, NULL, pDescriptorPool))

/**
 * Allocates one or more descriptor sets
 * @param pAllocateInfo is a pointer to a VkDescriptorSetAllocateInfo structure describing parameters of the allocation.
 * @param pDescriptorSets is a pointer to an array of VkDescriptorSet handles in which
 *              the resulting descriptor set objects are returned.
 */
#define ceAllocateDescriptorSets(pAllocateInfo, pDescriptorSets) \
        CHECK_VK(vkAllocateDescriptorSets(cenote_get_device(), pAllocateInfo, pDescriptorSets))

/**
 * Updates the contents of a descriptor set object
 * @param descriptorWriteCount is the number of elements in the pDescriptorWrites array.
 * @param pDescriptorWrites is a pointer to an array of VkWriteDescriptorSet structures describing the descriptor sets
 *              to write to.
 * @param descriptorCopyCount is the number of elements in the pDescriptorCopies array.
 * @param pDescriptorCopies is a pointer to an array of VkCopyDescriptorSet structures describing the descriptor sets
 *              to copy between.
 */
#define ceUpdateDescriptorSets(descriptorWriteCount, pDescriptorWrites, descriptorCopyCount, pDescriptorCopies) \
        vkUpdateDescriptorSets(cenote_get_device(), descriptorWriteCount, pDescriptorWrites, descriptorCopyCount, \
        pDescriptorCopies)


/**
 * Starts recording a command buffer
 * @param commandBuffer is the handle of the command buffer which is to be put in the recording state.
 * @param pBeginInfo is a pointer to a VkCommandBufferBeginInfo structure defining additional information
 * about how the command buffer begins recording.
 */
#define ceBeginCommandBuffer(commandBuffer, pBeginInfo) \
        CHECK_VK(vkBeginCommandBuffer(commandBuffer, pBeginInfo))


/**
 * Begins a new render pass
 * @param commandBuffer is the command buffer in which to record the command.
 * @param pRenderPassBegin is a pointer to a VkRenderPassBeginInfo structure specifying the render pass
 * to begin an instance of, and the framebuffer the instance uses.
 * @param contents is a VkSubpassContents value specifying how the commands in the first subpass will be provided.
 */
#define ceCmdBeginRenderPass(commandBuffer, pRenderPassBegin, contents) \
        vkCmdBeginRenderPass(commandBuffer, pRenderPassBegin, contents)


/**
 * Binds a pipeline object to a command buffer
 * @param commandBuffer is the command buffer that the pipeline will be bound to.
 * @param pipelineBindPoint is a VkPipelineBindPoint value specifying to which bind point the pipeline is bound.
 * Binding one does not disturb the others.
 * @param pipeline is the pipeline to be bound.
 */
#define ceCmdBindPipeline(commandBuffer, pipelineBindPoint, pipeline) \
        vkCmdBindPipeline(commandBuffer, pipelineBindPoint, pipeline)


/**
 * Sets the viewport dynamically for a command buffer
 * @param commandBuffer is the command buffer into which the command will be recorded.
 * @param firstViewport is the index of the first viewport whose parameters are updated by the command.
 * @param viewportCount is the number of viewports whose parameters are updated by the command.
 * @param pViewports is a pointer to an array of VkViewport structures specifying viewport parameters.
 */
#define ceCmdSetViewport(commandBuffer, firstViewport, viewportCount, pViewports) \
        vkCmdSetViewport(commandBuffer, firstViewport, viewportCount, pViewports)


/**
 * Sets scissor rectangles dynamically for a command buffer
 * @param commandBuffer is the command buffer into which the command will be recorded.
 * @param firstScissor is the index of the first scissor whose state is updated by the command.
 * @param scissorCount is the number of scissors whose rectangles are updated by the command.
 * @param pScissors is a pointer to an array of VkRect2D structures defining scissor rectangles.
 */
#define ceCmdSetScissor(commandBuffer, firstScissor, scissorCount, pScissors) \
        vkCmdSetScissor(commandBuffer, firstScissor, scissorCount, pScissors)


/**
 * Binds descriptor sets to a command buffer
 * @param commandBuffer is the command buffer that the descriptor sets will be bound to.
 * @param pipelineBindPoint is a VkPipelineBindPoint indicating the type of the pipeline that will use the descriptors.
 * There is a separate set of bind points for each pipeline type, so binding one does not disturb the others.
 * @param layout is a VkPipelineLayout object used to program the bindings.
 * @param firstSet is the set number of the first descriptor set to be bound.
 * @param descriptorSetCount is the number of elements in the pDescriptorSets array.
 * @param pDescriptorSets is a pointer to an array of handles to VkDescriptorSet objects describing the descriptor sets
 *                  to bind to.
 * @param dynamicOffsetCount is the number of dynamic offsets in the pDynamicOffsets array.
 * @param pDynamicOffsets is a pointer to an array of uint32_t values specifying dynamic offsets.
 */
#define ceCmdBindDescriptorSets(commandBuffer, pipelineBindPoint, layout, firstSet, descriptorSetCount, pDescriptorSets, \
dynamicOffsetCount, pDynamicOffsets) \
        vkCmdBindDescriptorSets(commandBuffer, pipelineBindPoint, layout, firstSet, descriptorSetCount, pDescriptorSets, \
dynamicOffsetCount, pDynamicOffsets)


/**
 * Ends the current render pass
 * @param commandBuffer is the command buffer in which to end the current render pass instance.
 */
#define ceCmdEndRenderPass(commandBuffer) \
        vkCmdEndRenderPass(commandBuffer)


/**
 * Finishes recording a command buffer
 * @param commandBuffer is the command buffer to complete recording.
 */
#define ceEndCommandBuffer(commandBuffer) \
        CHECK_VK(vkEndCommandBuffer(commandBuffer))


/**
 * Waits for one or more fences to become signaled
 * @param fenceCount is the number of fences to wait on.
 * @param pFences is a pointer to an array of fenceCount fence handles.
 * @param waitAll is the condition that must be satisfied to successfully unblock the wait.
 * If waitAll is VK_TRUE, then the condition is that all fences in pFences are signaled.
 * Otherwise, the condition is that at least one fence in pFences is signaled.
 * @param timeout is the timeout period in units of nanoseconds.
 * timeout is adjusted to the closest value allowed by the implementation-dependent timeout accuracy,
 * which may be substantially longer than one nanosecond, and may be longer than the requested period.
 */
#define ceWaitForFences(fenceCount, pFences, waitAll, timeout) \
        CHECK_VK(vkWaitForFences(cenote_get_device(), fenceCount, pFences, waitAll, timeout))


/**
 * Resets one or more fence objects
 * @param fenceCount is the number of fences to reset.
 * @param pFences is a pointer to an array of fence handles to reset.
 */
#define ceResetFences(fenceCount, pFences) \
        CHECK_VK(vkResetFences(cenote_get_device(), fenceCount, pFences))


/**
 * Resets a command buffer to the initial state
 * @param commandBuffer is the command buffer to reset.
 * The command buffer can be in any state other than pending, and is moved into the initial state.
 * @param flags is a bitmask of VkCommandBufferResetFlagBits controlling the reset operation.
 */
#define ceResetCommandBuffer(commandBuffer, flags) \
        CHECK_VK(vkResetCommandBuffer(commandBuffer, flags))


/**
 * Submits a sequence of semaphores or command buffers to a queue
 * @param queue is the queue that the command buffers will be submitted to.
 * @param submitCount is the number of elements in the pSubmits array.
 * @param pSubmits is a pointer to an array of VkSubmitInfo structures, each specifying a command buffer submission batch.
 * @param fence is an optional handle to a fence to be signaled once all submitted command buffers have completed execution.
 * If fence is not VK_NULL_HANDLE, it defines a fence signal operation.
 */
#define ceQueueSubmit(queue, submitCount, pSubmits, fence) \
        CHECK_VK(vkQueueSubmit(queue, submitCount, pSubmits, fence))


/**
 * Waits for a device to become idle
 */
#define ceDeviceWaitIdle() \
        CHECK_VK(vkDeviceWaitIdle(cenote_get_device()))


/**
 * Destroys a descriptor pool object
 * @param descriptorPool is the descriptor pool to destroy.
 */
#define ceDestroyDescriptorPool(descriptorPool) \
        vkDestroyDescriptorPool(cenote_get_device(), descriptorPool, NULL)


/**
 * Destroys a descriptor set layout object
 * @param descriptorSetLayout is the descriptor set layout to destroy.
 */
#define ceDestroyDescriptorSetLayout(descriptorSetLayout) \
        vkDestroyDescriptorSetLayout(cenote_get_device(), descriptorSetLayout, NULL)


/**
 * Destroys a fence object
 * @param fence is the handle of the fence to destroy.
 */
#define ceDestroyFence(fence) \
        vkDestroyFence(cenote_get_device(), fence, NULL)


/**
 * Destroys a semaphore object
 * @param semaphore is the handle of the semaphore to destroy.
 */
#define ceDestroySemaphore(semaphore) \
        vkDestroySemaphore(cenote_get_device(), semaphore, NULL)


/**
 * Destroys a command pool object
 * @param commandPool is the handle of the command pool to destroy.
 */
#define ceDestroyCommandPool(commandPool) \
        vkDestroyCommandPool(cenote_get_device(), commandPool, NULL)


/**
 * Destroys a framebuffer object
 * @param framebuffer is the handle of the framebuffer to destroy.
 */
#define ceDestroyFramebuffer(framebuffer) \
        vkDestroyFramebuffer(cenote_get_device(), framebuffer, NULL)


/**
 * Destroys a pipeline object
 * @param pipeline is the handle of the pipeline to destroy.
 */
#define ceDestroyPipeline(pipeline) \
        vkDestroyPipeline(cenote_get_device(), pipeline, NULL)


/**
 * Destroys a pipeline layout object
 * @param pipelineLayout is the pipeline layout to destroy.
 */
#define ceDestroyPipelineLayout(pipelineLayout) \
        vkDestroyPipelineLayout(cenote_get_device(), pipelineLayout, NULL)


/**
 * Destroys a render pass object
 * @param renderPass is the handle of the render pass to destroy.
 */
#define ceDestroyRenderPass(renderPass) \
        vkDestroyRenderPass(cenote_get_device(), renderPass, NULL)


/**
 * Creates a new sampler object
 * @param pCreateInfo is a pointer to a VkSamplerCreateInfo structure specifying the state of the sampler object.
 * @param pSampler is a pointer to a VkSampler handle in which the resulting sampler object is returned.
 */
#define ceCreateSampler(pCreateInfo, pSampler) \
        CHECK_VK(vkCreateSampler(cenote_get_device(), pCreateInfo, NULL, pSampler))


/**
 * Binds vertex buffers to a command buffer
 * @param commandBuffer is the command buffer into which the command is recorded.
 * @param firstBinding is the index of the first vertex input binding whose state is updated by the command.
 * @param bindingCount is the number of vertex input bindings whose state is updated by the command.
 * @param pBuffers is a pointer to an array of buffer handles.
 * @param pOffsets is a pointer to an array of buffer offsets.
 */
#define ceCmdBindVertexBuffers(commandBuffer, firstBinding, bindingCount, pBuffers, pOffsets) \
        vkCmdBindVertexBuffers(commandBuffer, firstBinding, bindingCount, pBuffers, pOffsets)


/**
 * Frees command buffers
 * @param commandPool is the command pool from which the command buffers were allocated.
 * @param commandBufferCount is the length of the pCommandBuffers array.
 * @param pCommandBuffers is a pointer to an array of handles of command buffers to free.
 */
#define ceFreeCommandBuffers(commandPool, commandBufferCount, pCommandBuffers) \
        vkFreeCommandBuffers(cenote_get_device(), commandPool, commandBufferCount, pCommandBuffers)


/**
 * Draws primitives
 * @param commandBuffer is the command buffer into which the command is recorded.
 * @param vertexCount is the number of vertices to draw.
 * @param instanceCount is the number of instances to draw.
 * @param firstVertex is the index of the first vertex to draw.
 * @param firstInstance is the instance ID of the first instance to draw.
 */
#define ceCmdDraw(commandBuffer, vertexCount, instanceCount, firstVertex, firstInstance) \
        vkCmdDraw(commandBuffer, vertexCount, instanceCount, firstVertex, firstInstance)


/**
 * Copies regions of an image, potentially performing format conversion
 * @param commandBuffer is the command buffer into which the command will be recorded.
 * @param srcImage is the source image.
 * @param srcImageLayout is the layout of the source image subresources for the blit.
 * @param dstImage is the destination image.
 * @param dstImageLayout is the layout of the destination image subresources for the blit.
 * @param regionCount is the number of regions to blit.
 * @param pRegions is a pointer to an array of VkImageBlit structures specifying the regions to blit.
 * @param filter is a VkFilter specifying the filter to apply if the blits require scaling.
 */
#define ceCmdBlitImage(commandBuffer, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions, filter) \
        vkCmdBlitImage(commandBuffer, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions, filter)


/**
 * Destroys a swapchain object
 * @param swapchain is the swapchain to destroy.
 */
#define ceDestroySwapchainKHR(swapchain) \
        vkDestroySwapchainKHR(cenote_get_device(), swapchain, NULL)


#endif //CENOTE_H
