//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CENOTE_CEFUNCTIONS_H
#define CENOTE_CEFUNCTIONS_H

#include "cetypes.h"





/**
 * Internal use only!
 * Checks the result of an expression.
 * If the result is different than VK_SUCCESS, print trace using CE_ERROR and exits.
 * This macro does not yield any result since the result of EXPR would always be VK_SUCCESS when exiting this macro.
 * To enforce a semicolon after this macro, we use an empty statement at the end of a statement expression rather than a
 * @code do{...} while(0) @endcode pattern, to be 100% sure it will be optimized away.
 */
#define private_SCHECK_VK(EXPR, SEED) ({                                                       \
    VkResult CA_UNIQUE_NAME(res, SEED) = (EXPR);                                                  \
    if (CA_UNIQUE_NAME(res, SEED) != VK_SUCCESS) {                                                \
        CA_ERROR(STRINGIFY(EXPR) " failed. Expected VK_SUCCESS, got %s\n",                     \
            cevkresulttoa(CA_UNIQUE_NAME(res, SEED)));                                            \
        exit(1);                                                                               \
    }                                                                                          \
    {}                                                                                         \
})

/**
 * Exits if EXPR is true.
 * @param EXPR the expression to evualate
 * @param MSG[in] [char*] the error message to print. <b>Must be preprocessor constant</b>.
 */
#define BEXIT(EXPR, MSG, ...) ({                                   \
    if ((EXPR)) {                                                  \
        CA_ERROR(STRINGIFY(EXPR) " is true| " MSG, ##__VA_ARGS__); \
        exit(1);                                                   \
    }                                                              \
    {}                                                             \
})



/**
 * Checks whether the result of an expression is <code>VK_SUCCESS</code>,
 * printing an error message then crashing if it is not equal.
 * @param EXPR the expression to checks. Must be yield a <code>VkResult</code> value.
 */
#define SCHECK_VK(EXPR) private_SCHECK_VK(EXPR, __COUNTER__)


#if DEBUG
/**
 * If DEBUG, checks whether the result of an expression is <code>VK_SUCCESS</code>,
 * printing an error message then crashing if it is not equal.
 * Otherwise, runs <code>EXPR</code> without checking anything.
 * @param EXPR the expression to checks. Must be yield a <code>VkResult</code> value.
 */
#   define CHECK_VK(EXPR) SCHECK_VK(EXPR)

/**
 * If DEBUG, expands <code>instr</code>, otherwise does nothing.
 * @param instr the line of code to be placed if and only if DEBUG is enabled
 */
#   define IFDEBUG(instr) instr;
#else
/**
 * If DEBUG, checks whether the result of an expression is <code>VK_SUCCESS</code>,
 * printing an error message then crashing if it is not equal.
 * Otherwise, runs <code>EXPR</code> without checking anything.
 * @param EXPR the expression to checks. Must be yield a <code>VkResult</code> value.
 */
#   define CHECK_VK(EXPR) EXPR

/**
 * If DEBUG, expands <code>instr</code>, otherwise does nothing.
 * @param instr the line of code to be placed if and only if DEBUG is enabled
 */
#   define IFDEBUG(instr)
#endif //DEBUG




#define private_TVkPhysicalDeviceShaderFloat16Int8Features VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES
#define private_TVkPhysicalDevice8BitStorageFeatures VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES
#define private_TVkPipelineShaderStageCreateInfo VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO
#define private_TVkPhysicalDeviceMaintenance4Features VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_FEATURES
#define private_TVkPhysicalDeviceMeshShaderFeaturesEXT VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_FEATURES_EXT
#define private_TVkFramebufferCreateInfo VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO
#define private_TVkRenderPassBeginInfo VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO
#define private_TVkPhysicalDeviceProperties2 VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2
#define private_TVkPhysicalDeviceSubgroupProperties VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_PROPERTIES
#define private_TVkPhysicalDeviceDescriptorIndexingFeaturesEXT \
VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES_EXT
#define private_TVkWriteDescriptorSet VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET
#define private_TVkSamplerCreateInfo VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO
#define private_TVkShaderModuleCreateInfo VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO
#define private_TVkImageCreateInfo VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO
#define private_TVkImageMemoryBarrier VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER
#define private_TVkBufferCreateInfo VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO
#define private_TVkCommandBufferAllocateInfo VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO
#define private_TVkSemaphoreCreateInfo VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
#define private_TVkFenceCreateInfo VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
#define private_TVkCommandPoolCreateInfo VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO
#define private_TVkImageViewCreateInfo VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO
#define private_TVkDescriptorSetAllocateInfo VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO
#define private_TVkDescriptorPoolCreateInfo VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO
#define private_TVkPipelineDynamicStateCreateInfo VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO
#define private_TVkPipelineColorBlendStateCreateInfo VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO
#define private_TVkPipelineMultisampleStateCreateInfo VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO
#define private_TVkPipelineDepthStencilStateCreateInfo VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO
#define private_TVkPipelineRasterizationStateCreateInfo VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO
#define private_TVkPipelineViewportStateCreateInfo VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO
#define private_TVkPipelineTessellationStateCreateInfo VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO
#define private_TVkPipelineInputAssemblyStateCreateInfo VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO
#define private_TVkPipelineVertexInputStateCreateInfo VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO
#define private_TVkGraphicsPipelineCreateInfo VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO
#define private_TVkPipelineLayoutCreateInfo VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO
#define private_TVkDescriptorSetLayoutCreateInfo VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO
#define private_TVkSubmitInfo VK_STRUCTURE_TYPE_SUBMIT_INFO
#define private_TVkPresentInfoKHR VK_STRUCTURE_TYPE_PRESENT_INFO_KHR
#define private_TVkCommandBufferBeginInfo VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
#define private_TVkSwapchainCreateInfoKHR VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR
#define private_TVkApplicationInfo VK_STRUCTURE_TYPE_APPLICATION_INFO
#define private_TVkDeviceQueueCreateInfo VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO
#define private_TVkDeviceCreateInfo VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO
#define private_TVkInstanceCreateInfo VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO
#define private_TVkDebugUtilsMessengerCreateInfoEXT VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT
#define private_TVkValidationFeaturesEXT VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT
#define sTypeDefault(strukt) sType = private_T##strukt


const char* cevkresulttoa(VkResult r);
void cereadbytes(array *byteBuffer, const char *filename);
#endif //CENOTE_CEFUNCTIONS_H
