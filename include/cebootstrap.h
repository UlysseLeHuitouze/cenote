//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CENOTE_CEBOOTSTRAP_H
#define CENOTE_CEBOOTSTRAP_H

#include "cetypes.h"
#include "cefunctions.h"



/**
 * Notes:
 * To make shader printf work, you need to:
 * - Enable instance layer VK_LAYER_KHRONOS_validation, if not already done for debug callback
 * - Extends VkInstanceCreateInfo in pNext with a VkValidationFeaturesEXT structure, followed by the usual
 *   VkDebugUtilsMessengerCreateInfoEXT structure. VkDebugUtilsMessengerCreateInfoEXT pNext must be NULL, so
 *   VkValidationFeaturesEXT must be put before in the pNext chain.
 * - In that very VkValidationFeaturesEXT structure, set pEnabledValidationFeatures to
 *   VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT, and increment enabledValidationFeatureCount.
 * - Make sure the validation debug callback shows messages with severity VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT.
 */



/**
 * Deletes the old swapchain in <code>swapchainCreateInfo.oldSwapchain</code>, creates a new one, stores it into
 * <code>swapchainCreateInfo.oldSwapchain</code> and retrieves the new swapchain images into <code>swapchainImages</code>.
 * @param physicalDevice[in] a <code>VkPhysicalDevice</code> handle
 * @param device[in] a <code>VkDevice</code> handle
 * @param swapchainCreateInfo[in,out] a pointer to a <code>VkSwapchainCreateInfoKHR</code> struct
 * @param allocator[in] NULL or a pointer to a <code>VkAllocationCallbacks</code> struct
 * @param swapchainImages[in] an array to write swapchain images into.
 * Must be able to hold <code>swapchainCreateInfo.minImageCount</code> VkImages.
 */
void cemakeswapchain(
        VkPhysicalDevice physicalDevice,
        VkDevice device,
        VkSwapchainCreateInfoKHR *swapchainCreateInfo,
        const VkAllocationCallbacks *allocator,
        VkImage *swapchainImages);


/**
 * Initializes Vulkan API with the given inputs, retrieving the instantiated Vulkan instance,
 * debug messenger, physical device, logical device and the surface.
 * @param input[in,out] a collection of parameters for customizing vulkan initialization.
 *              Refers to VulkanSetupInput struct documentation for how to fill it correctly.
 * @param output[out] a collection of pointers to Vulkan handles, for retrieving the created Vulkan handles.
 *              Refers to VulkanSetupOutput struct documentation for how to fill it correctly.
 */
void ceinitvk(VulkanSetupInput *input, VulkanSetupOutput *output);


/**
 * Destroys main Vulkan bootstrap objects in order, that is:
 * <ol>
 *      <li>VkDevice</li>
 *      <li>VkSurfaceKHR</li>
 *      <li>VkDebugUtilsMessengerEXT</li>
 *      <li>VkInstance</li>
 * </ol>
 * @param vkInstance[in] a vulkan instance
 * @param allocator[in] NULL or a pointer to a <code>VkAllocationCallbacks</code> struct
 * @param debugMessenger[in] VK_NULL_HANDLE or a <code>VkDebugUtilsMessengerEXT</code> handle
 * @param surface[in] a <code>VkSurfaceKHR</code> handle
 * @param device[in] a <code>VkDevice</code> handle
 */
void cedestroyvk(
        VkInstance vkInstance,
        const VkAllocationCallbacks *allocator,
        VkDebugUtilsMessengerEXT debugMessenger,
        VkSurfaceKHR surface,
        VkDevice device);


#endif //CENOTE_CEBOOTSTRAP_H