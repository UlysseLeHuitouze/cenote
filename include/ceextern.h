//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CENOTE_CEEXTERN_H
#define CENOTE_CEEXTERN_H


#include "Volk/volk.h"
#include <stdlib.h>
#if __STDC_VERSION__ <= 201710
#   include <stdbool.h>
#endif
#include <vma/vk_mem_alloc.h>
#include "stb_image.h"
#include "carig.h"


#endif //CENOTE_CEEXTERN_H
