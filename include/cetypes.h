//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CENOTE_CETYPES_H
#define CENOTE_CETYPES_H

#include "ceextern.h"



typedef struct CeBuffer {
    VkBuffer vkBuffer;
    VmaAllocation vmaAllocation;
} CeBuffer;

typedef struct CeImage {
    VkImage vkImage;
    VmaAllocation vmaAllocation;
} CeImage;

typedef struct CeTexture {
    CeImage ceImage;
    VkImageView vkImageView;
} CeTexture;

typedef struct CeSampledTexture {
    CeTexture ceTexture;
    VkSampler vkSampler;
} CeSampledTexture;


/**
 * Describes the preferred option for the swapchain.
 * Used in <code>ceinitvk</code>.
 * <code>imageCount</code> is sure to be uphold.
 * The other fields may not directly translate to the swapchain actual configuration.
 */
typedef struct SwapchainSetupInfo {
    VkSurfaceFormatKHR surfaceFormat;
    VkPresentModeKHR presentMode;
    VkImageUsageFlags imageUsage;
    uint32_t imageCount;
} SwapchainSetupInfo;










/**
 * A temporary placeholder to hold the resulting queues and queue families from a call to <code>ceinitvk</code>.
 * Its field must be pre-allocated, i.e.
 * @code
 *  QueueFeatures qf = {
 *      .queues = (VkQueue[NQUEUES]){},
 *      .queueFamilyIndices = (uint32_t[NQUEUES]){},
 *      .capacity = NQUEUES
 *  };
 * @endcode
 * rather than just
 * @code
 *  QueueFeatures qf = {};
 * @endcode
 *
 * See <code>VulkanSetupInput</code>, <code>VulkanSetupOutput</code> and <code>QueueFamilySelectInfo</code> documentation
 * for details.
 */
typedef struct QueueFeatures {
    /**
     * Placeholder for the resulting queues from a call to <code>ceinitvk</code>.
     * Its length must match <code>queueFamilyIndices</code> field's length.
     */
    VkQueue *queues;
    /**
     * Placeholder for the resulting queue indices for each corresponding queues in <code>queues</code> field,
     * from a call to <code>ceinitvk</code>.
     * Its length must match <code>queues</code> field's length.
     */
    uint32_t *queueFamilyIndices;
    /**
     * The total number of queues requested by the application when calling <code>ceinitvk</code>.
     * Length of both <code>queues</code> and <code>queueFamilyIndices</code> fields.
     */
    uint32_t capacity;
} QueueFeatures;










/**
 * Describes how queue family should be selected for each queue needed by the application.
 * Used <code>VulkanSetupInput</code> struct for calling <code>ceinitvk</code>. Selects what to write in the
 * <code>QueueFeatures</code> in <code>VulkanSetupOutput</code> for a call to <code>ceinitvk</code>.<br><br>
 * <p>
 * Concretely, regarding queue family filtering, the implementation of <code>ceinitvk</code> will behave as:
 * @code
 *  void selectQueueFamilies(VulkanSetupInput *in, VulkanSetupOutput *out) {
 *
 *      QueueFamilySelectInfo selectInfo = *in-&gt;requestedQueuesInfo;
 *      QueueFeatures *queueFeatures = out-&gt;queueFeatures;
 *      int queueCount = selectInfo.queueFamiliesFallback.len;
 *
 *      // first pass. fill with beacon value -1
 *      for (int i = 0; i &lt; queueCount; i++)
 *          queueFeatures-&gt;queueFamilyIndices[i] = -1;
 *
 *      int availableQueueFamilyCount = ...
 *      VkQueueFamilyProperties *availableQueueFamilyProps = ...
 *
 *      // second pass. do the actual filtering
 *      for (int i = 0; i &lt; availableQueueFamilyCount; i++) {
 *          for (int j = 0; j &lt; queueCount; j++)
 *              if (queueFeatures-&gt;queueFamilyIndices[j] == -1 &&
 *                  selectInfo.queueFamiliesSelectPredicate[j](
 *                          availableQueueFamilyProps[i],
 *                          doesFamilySupportSurface(i)
 *                  )
 *              )
 *                  queueFeatures-&gt;queueFamilyIndices[j] = i;
 *      }
 *
 *      uint32_t *fallback_indices = selectInfo.queueFamiliesFallback.elements;
 *
 *      // third pass. fallback if there are still some -1 lurking
 *      for (int i = 0; i &lt; queueCount; i++)
 *
 *          if (queueFeatures-&gt;queueFamilyIndices[i] == -1) {
 *              // falling back
 *              uint32_t idx = queueFeatures-&gt;queueFamilyIndices[fallback_indices[i]];
 *              if (idx == -1)
 *                  idx = 0;    // fallback failed
 *              queueFeatures-&gt;queueFamilyIndices[i] = idx;
 *          }
 *
 *  }
 * @endcode
 * </p>
 * This behaviour only allows for a partial two-level fallback strategy: Let A, B two indices in
 * queueFamiliesSelectPredicate such that A &lt; B and queueFamiliesFallback.elements[A] = B. Let's say both A and B
 * still need to fallback after the second pass. Then B will be able to fallback accordingly,
 * but A won't be able to fallback to what B will later fallback to, and will instead end up being equal to 0
 * (even if B successfully falls back later on).
 */
typedef struct QueueFamilySelectInfo {
    /**
     * A valid array of predicate for filtering queue families out.
     * <ul>
     *  <li>First param is a <code>VkQueueFamilyProperties</code> structure.</li>
     *  <li>Second param is a boolean that tells whether the queue family supports window surface operations.</li>
     * </ul>
     * @return true when a queue family seems suited for a VkQueue requirements, false otherwise
     * @note Its length must match <code>queueFamiliesFallback.len</code>.
     */
    bool (**queueFamiliesSelectPredicate)(VkQueueFamilyProperties, VkBool32);
    /**
     * <p>An array of integers to specify which queue family to fallback to when failing to find a suitable queue family.
     * </p>
     * <p>
     * More precisely, if <code>ceinitvk</code> fails to find a queue family that satisfies
     * <code>queueFamiliesSelectPredicate[i]</code>, it will fallback to the queue family which satisfied
     * <code>queueFamiliesSelectPredicate[queueFamiliesFallback.elements[i]]</code>. If no queue family satisfied
     * <code>queueFamiliesSelectPredicate[queueFamiliesFallback.elements[i]]</code> yet, it will fallback to the
     * queue family at index 0.
     * </p>
     */
    array queueFamiliesFallback;
} QueueFamilySelectInfo;










/**
 * Represents the inputs needed to setup a minimal vulkan application, i.e. creating an instance,
 * creating a debug messenger if DEBUG,
 * selecting a physical device, selecting queue families suited for the application queues,
 * creating a logical device, creating a surface and pre-filling a SwapchainCreateInfo structure.<br><br>
 * Used by <code>ceinitvk</code> in <code>cebootstrap.h</code>.
 */
typedef struct VulkanSetupInput {
    /**
     * A pointer to a vanilla <code>VkApplicationInfo</code> struct.
     * Must abide <code>VkInstanceCreateInfo</code>'s specification.
     */
    VkApplicationInfo *appInfo;
    /**
     * A pointer to a vanilla <code>VkAllocationCallbacks</code> struct.
     * Must abide <code>vkCreate*</code> functions' specification.
     */
    VkAllocationCallbacks *allocCallbacks;
    /**
     * An array of strings containing the Vulkan layers requested by the application.<br>
     * Its underlying array must be non-NULL, i.e. <code>requestedLayers.elements != NULL</code>.<br><br>
     * To ensure the above, do initialize like so:
     * @code requestedLayers  = ca_array_of(char*, ...) @endcode
     * rather than
     * @code requestedLayers = {} @endcode
     */
    array requestedLayers;
    /**
     * An array of strings containing the Vulkan instance extensions requested by the application.<br>
     * Its underlying array must be non-NULL, i.e. <code>requestedInstanceExtensions.elements != NULL</code>.<br><br>
     * To ensure the above, do initialize like so:
     * @code requestedInstanceExtensions  = ca_array_of(char*, ...) @endcode
     * rather than
     * @code requestedInstanceExtensions = {} @endcode
     */
    array requestedInstanceExtensions;
    /**
     * An array of strings containing the Vulkan device extensions requested by the application.<br>
     * Its underlying array must be non-NULL, i.e. <code>requestedDeviceExtensions.elements != NULL</code>.<br><br>
     * To ensure the above, do initialize like so:
     * @code requestedDeviceExtensions  = ca_array_of(char*, ...) @endcode
     * rather than
     * @code requestedDeviceExtensions = {} @endcode
     */
    array requestedDeviceExtensions;
    /**
     * NULL or a pointer to a structure extending VkDeviceCreateInfo structure.
     * Must abide <code>VkDeviceCreateInfo</code>'s specification.
     */
    void *pNextDeviceCreateInfo;
    /**
     * The name or a substring of the name of the GPU requested by the application.<br>
     * <b>Must not be NULL</b>.<br>
     * Note <code>GPUName = ""</code> will validate any GPU, selecting the first one on the computer.
     */
    const char *GPUName;
    /**
     * NULL or a valid pointer to a function for retrieving extra required instance extensions,
     * usually coming from your Window System Integration library (e.g. GLFW, SDL).<br><br>
     * <ul>
     * <li>First param is a valid pointer to write the number of extra extensions into.</li>
     * <li>Second param is a valid pointer to write the pointer to the extra extensions into.</li>
     * </ul>
     * If retrieving those extra instance extensions requires you to manually free them (cf. SDL), consider using
     * <code>freeInstanceExtensions_ptr</code> neighbour field in this structure for that purpose.
     * @return VK_SUCCESS on success, anything else otherwise
     * @example (using GLFW) @code
     *  static VkResult getRequiredInstanceExtensions(uint32_t *count, const char ***ext) {
     *      const char **glfwExt = glfwGetRequiredInstanceExtensions(count);
     *      if (glfwExt == NULL)
     *          return VK_ERROR_INITIALIZATION_FAILED;
     *
     *      *ext = glfwExt;
     *      return VK_SUCCESS;
     *  }
     * @endcode
     */
    VkResult (*getInstanceExtensions_ptr)(uint32_t*, const char***);
    /**
     * NULL or a valid pointer to a function for freeing extra instance extensions that were retrieved with
     * <code>getInstanceExtensions_ptr</code>. If your Window System Integration library (e.g. GLFW) doesn't require
     * you to manually free those extra extensions, leaving this field NULL is what you want to do.<br><br>
     * Note that if <code>getInstanceExtensions_ptr</code> neighbour field is NULL, this function won't be called,
     * even if it is not NULL.
     */
    void (*freeInstanceExtensions_ptr)(const char**);
    /**
     * A valid pointer to a function for creating a <code>VkSurfaceKHR</code> object,
     * usually provided by your Window System Integration library (e.g. GLFW, SDL).<br><br>
     * <ul>
     * <li>First param is a valid <code>VkInstance</code> handle.</li>
     * <li>Second param is NULL or a pointer to a <code>VkAllocationCallbacks</code> struct.</li>
     * <li>Third param is a valid pointer to a <code>VkSurfaceKHR</code> handle to write into.</li>
     *
     * </ul>
     *
     * Since this method will be called internaly at some point in <code>cebootstrap.c</code>, the application's window
     * must have been created before calling filling this structure and calling <code>ceinitvk</code>.
     *
     * <h3>Important note</h3>
     * <br><br>
     * <code>cebootstrap.h</code> is designed for single-surface single-device vulkan instance applications.
     * The application can still call <code>ceinitvk</code> more than once, resulting in several instances being created,
     * but it doesn't allow the application to create multiple devices and multiple surfaces per instance.<br>
     * If your application needs the later, and cloning vulkan instances is too much overhead, consider using another
     * bootstrapping library.
     *
     * @return VK_SUCCESS upon success, anything else otherwise
     * @example (using GLFW) @code
     *  static VkResult createVkSurfaceKHR(VkInstance instance,
     *                                     const VkAllocationCallbacks *allocator,
     *                                     VkSurfaceKHR *surface) {
     *      return glfwCreateWindowSurface(instance, window, allocator, surface);
     *  }
     * @endcode where <code>window</code> is a static variable of type <code>GLFWwindow*</code>
     * representing the application's window.
     */
    VkResult (*createVkSurfaceKHR_ptr)(VkInstance, const VkAllocationCallbacks *, VkSurfaceKHR *);
    /**
     * A valid pointer to a <code>VkPhysicalDeviceFeatures</code> struct.<br>
     * Must abide by <code>VkDeviceCreateInfo</code>'s specification.
     */
    VkPhysicalDeviceFeatures *requestedDeviceFeatures;
    /**
     * A valid pointer to a <code>QueueFamilySelectInfo</code> struct, describing the requirements
     * for the queues needed by the application.<br>
     * <ul>
     * <li>Its field <code>queueFamiliesSelectPredicate</code> must be a valid array of valid pointers to function
     * for determining whether a queue family is suited for one of your queue.</li>
     * <li>Its field <code>queueFamiliesFallback</code> must be a valid array of integers, i.e.
     * <code>queueFamiliesFallback.elements != NULL</code>.</li>
     * <li>Its field <code>queueFamiliesSelectPredicate</code> length must match its field <code>queueFamiliesFallback</code>
     * length, i.e. there should be <code>queueFamiliesFallback.len</code> predicates in
     * <code>queueFamiliesSelectPredicate</code>.</li>
     * </ul>
     * See <code>QueueFamilySelectInfo</code> for a concrete example.
     */
    QueueFamilySelectInfo *requestedQueuesInfo;
    /**
     * A valid pointer to a <code>SwapchainSetupInfo</code> struct, describing the requirements
     * for the swapchain needed by the application.<br>
     * The application may end up with an eerie swapchain if its field are not all set properly.
     */
    SwapchainSetupInfo *requestedSwapchainInfo;
    /**
     * A valid pointer to a <code>VmaAllocatorCreateInfo</code> struct, describing the requirements
     * for the allocator needed by the application.<br>
     * Must abide by <code>VmaAllocatorCreateInfo</code>'s specification.
     */
    VmaAllocatorCreateInfo *requestedAllocatorInfo;
} VulkanSetupInput;










/**
 * Represents the outputs of initializing a minimal vulkan application, i.e. stores an instance,
 * a debug messenger, a physical device, a logical device, a <code>QueueFeatures</code> struct
 * containing all necessary information related to queues, a <code>VkSwapchainCreateInfoKHR</code> struct which
 * stores the created surface in its field <code>surface</code>, and an allocator.
 * <br><br>
 * Used by <code>ceinitvk</code> in <code>cebootstrap.h</code>.
 */
typedef struct VulkanSetupOutput {
    /**
     * A valid pointer to a VkInstance variable to write into when creating vulkan's instance.
     */
    VkInstance *instance;
    /**
     * A valid pointer to a VkDebugUtilsMessengerEXT variable to write into when creating vulkan's debug messenger.
     */
    VkDebugUtilsMessengerEXT *debugMessenger;
    /**
     * A valid pointer to a VkPhysicalDevice variable to write into when selecting a GPU.
     */
    VkPhysicalDevice *physicalDevice;
    /**
     * A valid pointer to a VkDevice variable to write into when creating vulkan's logical device.
     */
    VkDevice *device;
    /**
     * <p>A valid pointer to a QueueFeatures struct to write into when creating queues.
     * Won't be allocated by <code>ceinitvk</code>, thus it <b>must</b> pre-allocated before
     * calling <code>ceinitvk</code>, i.e. its fields <code>queues</code> and <code>queueFamilyIndices</code>
     * must be valid arrays, which capacity must match its field <code>capacity</code>.
     * </p>
     * <p>Furthermore, this aforementioned <code>capacity</code> field must match with the field
     * <code>requestedQueuesInfo</code> in the structure <code>VulkanSetupInput</code> when calling <code>ceinitvk</code>.
     * </p>
     *
     * <p>Concretely, for any call <code>ceinitvk(&input, &output)</code>,
     * @code input.requestedQueuesInfo->queueFamiliesFallback.len = output.queueFeatures->capacity @endcode
     * must hold.</p>
     *
     * After successfully exiting <code>ceinitvk(&input, &output)</code>, the following will hold:
     * <ul>
     *  <li><code>for all i. 0 &lt;= i &lt; output-&gt;queueFeatures.capacity → </code>
     *  "<i>output-&gt;queueFeatures.queues[i] is a valid handle to a VkQueue object,
     *      belonging to the queue family of index output-&gt;queueFeatures.queueFamilyIndices[i].</i>"</li>
     *
     *  <li><code>for all i. 0 &lt;= i &lt; output-&gt;queueFeatures.capacity → </code>
     *  "<i>when <code>ceinitvk</code> managed to find a queue family satisfying
     *  input-&gt;requestedQueuesInfo.queueFamiliesSelectPredicate[i], the queue family of index
     *  output-&gt;queueFeatures.queueFamilyIndices[i] is sure to satisfy
     *  input-&gt;requestedQueuesInfo.queueFamiliesSelectPredicate[i] too.</i>"</li>
     *
     *  <li><code>for all i. 0 &lt;= i &lt; output-&gt;queueFeatures.capacity → </code>
     *  "<i>when <code>ceinitvk</code> did <b>not</b> managed to find a queue family satisfying
     *  input-&gt;requestedQueuesInfo.queueFamiliesSelectPredicate[i],
     *  output-&gt;queueFeatures.queueFamilyIndices[i] is either 0 or
     *  output-&gt;queueFeatures.queueFamilyIndices[k], depending on whether k is -1 or not respectively, where
     *  k is input-&gt;requestedQueuesInfo.queueFamiliesFallback.elements[i].</i>"</li>
     * </ul>
     */
    QueueFeatures *queueFeatures;
    /**
     * A valid pointer to a VkSwapchainCreateInfoKHR struct to write into when creating vulkan's surface.
     */
    VkSwapchainCreateInfoKHR *swapchainInfo;
    /**
     * A valid pointer to a VmaAllocator variable to write into when creating vma's allocator.
     */
    VmaAllocator *allocator;
} VulkanSetupOutput;


#endif //CENOTE_CETYPES_H
