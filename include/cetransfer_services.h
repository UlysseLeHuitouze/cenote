//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CENOTE_CETRANSFER_SERVICES_H
#define CENOTE_CETRANSFER_SERVICES_H


#include "ceextern.h"
#include "cetypes.h"
#include "cefunctions.h"


/**
 * Primes Cenote transfer service by specifying the VkQueue object used for transferring, the command pool to allocate
 * command buffers from as well as the amount of buffers that can run asynchronously.
 * Cenote transfer service must have never been primed before or must be killed via #siTSKillService to be prime again.
 * @param device [in] vulkan device
 * @param transferQueue [in] the queue to use for transfer
 * @param allocator [in] vma's allocator to allocate staging buffers from
 * @param transferPool [in] the pool to allocate command buffers for transfer from
 * @param uploadBufferingCount [in] the amount of staging buffers and command buffers to allocate. 2 is a good option
 */
void cetsprime(VkDevice device,
                      VkQueue transferQueue,
                      VmaAllocator allocator,
                      VkCommandPool transferPool,
                      uint32_t uploadBufferingCount);

/**
 * Kills Cenote transfer service, i.e. destroys all the staging buffers and frees all the command buffers.
 * #siTSPrimeService must have been called exactly once after the last time this method was called.
 * After calling this method, #siTSPrimeService can be called again.
 */
void cetskill();

/**
 * Stages <code>pData</code> to the buffer <code>dst</code>, which is on the GPU.
 * @param pData a pointer to the data to upload to the GPU
 * @param dst the destination to stage to
 * @param dstOffset the offset in the destination to start writing from
 * @param size the size of the upload
 */
void cetstobuffer(void *pData, VkBuffer dst, VkDeviceSize dstOffset, VkDeviceSize size);

/**
 * Stages <code>pData</code> to the image <code>dst</code>, which is on the GPU.
 * @param pData [in] a pointer to the image data
 * @param dst [in] the destination image
 * @param bufferImageCopy [in] buffer-to-image copy information, such as image extent, buffer region, etc.
 * @param subresourceRange [in] subresource range for <code>dst</code>
 * @param finalLayout [in] <code>dst</code> final layout (e.g. VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL for textures)
 * @param dstAccessMask [in] <code>dst</code> destination access mask (e.g. VK_ACCESS_SHADER_READ_BIT for texture)
 * @param dstStageMask [in] pipeline stage mask where <code>dst</code> will be used
 * @param size [in] the size of <code>pData</code> to copy. Must match with <code>bufferImageCopy</code>.
 */
void cetstoimage(const void *pData,
                      VkImage dst,
                      const VkBufferImageCopy *bufferImageCopy,
                      const VkImageSubresourceRange *subresourceRange,
                      VkImageLayout finalLayout,
                      VkAccessFlags dstAccessMask,
                      VkPipelineStageFlags dstStageMask,
                      VkDeviceSize size);

/**
 * Stages a texture loaded from <code>filepath</code> using standard R8G8B8A8 format.<br>
 * Vulkan-side, the actual format used is VK_FORMAT_R8G8B8A8_SRGB for optimal colors.<br>
 * Does the following:
 * <ul>
 *      <li>Load data at <code>filepath</code> using <code>stbi</code>.</li>
 *      <li>Creates a device-local image large enough to host the texture data,
 *      which handle is written in <code>texture->siImage</code>.</li>
 *      <li>Stages the texture data in the image at the base mipmap level.</li>
 *      <li>Generates the other mipmap levels if <code>mipmap_enabled</code> is true by downscaling the base mipmap level image.</li>
 *      <li>Makes the final image <code>VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL</code>.</li>
 *      <li>Creates the correct image view, which handle is written in <code>texture->imageView</code>.</li>
 * </ul>
 *
 * @param filepath [in] the texture file path
 * @param texture [out] the texture structure to fill
 * @param dstStageMask [in] the pipeline stages where the texture will be used
 * @param mipmap_enabled [in] whether mipmap levels should be generated
 */
void cetstotexture(const char *filepath, CeTexture *texture, VkPipelineStageFlags dstStageMask, bool mipmap_enabled);

/**
 * * Stages a texture loaded from <code>filepath</code> using standard R8G8B8A8 format.<br>
 * Essentially
 * @code
 * siTSStageTexture(filepath, texture, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 1);
 * @endcode
 * @param filepath [in] the texture file path
 * @param texture [out] the texture structure to fill
 * @see #siTSStageTexture
 */
void cetstotexture_default(const char *filepath, CeTexture *texture);

#endif //CENOTE_CETRANSFER_SERVICES_H
